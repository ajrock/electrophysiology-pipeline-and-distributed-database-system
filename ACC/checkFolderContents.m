function [matFile] = checkFolderContents(inputFolder)
%checkFolderContents Summary of this function goes here
%   Detailed explanation goes here

if nargin == 0
    inputFolder = uigetdir(cd);
end

contents = dir(inputFolder);

srcList = contents(contains({contents.name},'src'));

matFileStatus = contents(contains({contents.name},'folderData.mat'));

if isempty(matFileStatus) % Create new mat file
    fileName = {srcList.name}';
    analysisDone = repmat({'no'},length(fileName),1);
    analysisUsed = repmat({'maybe'},length(fileName),1);
    matFile = table(fileName,analysisDone,analysisUsed);
    save([inputFolder '\folderData'],'matFile')
else
    load([inputFolder '\' matFileStatus(1).name])
end

end

