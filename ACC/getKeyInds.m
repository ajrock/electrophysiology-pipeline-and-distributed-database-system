function [inds] = getKeyInds(fileType,paramNames,fileName)
%getKeyInds Summary of this function goes here
%   Detailed explanation goes here
try
switch fileType
    case 'IO'
        %inds(1)  = find(contains(paramNames,'dB SPL'));
        if contains(fileName,'el','IgnoreCase',1)
            inds(1)  = find(contains(paramNames,{'S1_att'}),1);
            inds(2)  = find(contains(paramNames,{'S2_att'}),1);
        else
            inds(1)  = find(contains(paramNames,{'Atten1 [dB]','Atten2 [dB]'}));
            inds(2)  = find(contains(paramNames,{'Atten3 [dB]','Atten4 [dB]'}));
        end
    case 'ITD'
        inds(1) = find(contains(paramNames,'itd')&~contains(paramNames,{'itd_center','itdCenter'}));
    case 'RRTF'
        if contains(fileName,'el','IgnoreCase',1)
            inds(1)  = find(contains(paramNames,'S1_pulses_n'));
            inds(2)  = find(contains(paramNames,'S2_pulses_n'));
            inds(3)  = find(contains(paramNames,'S1_IPI'));
            inds(4)  = find(contains(paramNames,'S2_IPI'));
        else
            inds(1)  = find(contains(paramNames,{'click_n','pulseCount'}));
            inds(2)  = find(contains(paramNames,{'S1_clickrate','ipiLeft'}));
            inds(3)  = find(contains(paramNames,{'S2_clickrate','ipiRight'}));
        end
    case 'TC'
        % Case of 2 dB SPL levels
        if length(find(contains(paramNames,'dB SPL')))==2
            inds(1)  = find(contains(paramNames,'S1 dB SPL'));
            inds(4)  = find(contains(paramNames,'S2 dB SPL'));
            inds(2)  = find(contains(paramNames,'S1_freq'));
            inds(3)  = find(contains(paramNames,'S2_freq'));
        else
            inds(1)  = find(contains(paramNames,'dB SPL'));
            inds(2)  = find(contains(paramNames,{'S1_freq', 'freq_1'}));
            inds(3)  = find(contains(paramNames,{'S2_freq', 'freq_2'}));
            inds(4)  = inds(1);
        end
%         assert(~isempty(splInd) & ~isempty(freqInd),'Invalid Indices!')
%         [levels,b1,splInds] = unique(stimGrid(splInd,:));
%         [freqs,b2,freqInds] =  unique(stimGrid(freqInd,:));
    otherwise
        %error('Bad Folder Selection! Folder must contain either IO, ITD, RRTF, or TC, ONLY!')
end
catch
    sprintf('Could not find valid index for values! \nPlease check the correct indices for the following approximate names: ')
    switch fileType
        case 'IO'
            attNames = {'Left Attenuator (1 or 2 usually)','Right Attenuator (3 or 4 usually)'};
        case 'ITD'
            attNames = {'ITD values','ITD center (usually ~15)'};
        case 'RRTF'
            attNames = {'number of pulses or clicks','Left pulse Rate','Right pulse Rate'};
        case 'TC'
            attNames = {'db SPL level','Left Freq','Right Freq'};
        otherwise
            %error('Bad Folder Selection! Folder must contain either IO, ITD, RRTF, or TC, ONLY!')
    end
    sprintf('\n%s ', attNames{:})
    %fig = stimGridSelection(fileType,paramNames,fileName);
    fig = radioSelection({fileName,paramNames{:}});
    inds = find(endsWith(paramNames,sort(guidata(fig))));
    delete(fig)
end
%for ii = 1:length(inds)
    
end