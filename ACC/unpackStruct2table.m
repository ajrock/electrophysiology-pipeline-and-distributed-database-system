function cellTab = unpackStruct2table(data,prevName)
%UNPACKSTRUCT2TABLE transform nested structure of unequal field sizes to a
%table that can be written to excel
fn = fieldnames(data);

cellTab = {};
thisTab = table();

for ii = 1:length(fn)
    if isstruct(data.(fn{ii}))
        if exist('prevName')
            cellTab = [cellTab unpackStruct2table(data.(fn{ii}),[prevName '_' fn{ii}])];
        else
            cellTab = [cellTab unpackStruct2table(data.(fn{ii}),fn{ii})];
        end
    else
        if (~iscell(data.(fn{ii})) && ndims(data.(fn{ii})) <= 2 && numel(data.(fn{ii})) <=2) || ...
                ischar(data.(fn{ii})) || isstring(data.(fn{ii}))
            if strcmp(class(data.(fn{ii})),'cfit')
                fit.a = data.(fn{ii}).a;
                fit.b = data.(fn{ii}).b;
                fit.c = data.(fn{ii}).c;
                fit.d = data.(fn{ii}).d;
                fit.e = data.(fn{ii}).e;
                data.(fn{ii}) = fit;
            end
            if exist('prevName')
                thisTab = [thisTab, table({data.(fn{ii})},'VariableNames',{[prevName '_' fn{ii}]})];
            else
                thisTab = [thisTab, table({data.(fn{ii})},'VariableNames',fn(ii))];
            end
        end
    end
end

if exist('prevName')
    thisTab = [table({prevName},'VariableNames',{'TableTitle'}) thisTab];
else
    thisTab = [table({'Meta'},'VariableNames',{'TableTitle'}) thisTab];
end

cellTab = [cellTab, {thisTab}];

end
