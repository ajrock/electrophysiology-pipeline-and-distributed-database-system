function success = saveAllFigs(saveFolder,formatO)
%saveAllFigs Cheater function that reads a list of all figures from the
%workspace and saves them to a desired folder
%   Makes ugly ugly use of the evalin function to save all the figures
%   'for' the other function to the folder specified. May add modularity
%   for save types later, but considering most matlab experts consider what
%   I am doing now a terrible idea anyways... yeah

% evalin allows the function to read variables from the caller's workspace,
% normally this is highly unrecommended!
%allFigs=evalin('caller', 'whos(''-regexp'', ''[f F]ig$'')');
%allFigs(~cellfun(@(x) strcmp(x,'matlab.ui.Figure'),{allFigs.class}))=[];

if ~strcmp(saveFolder(end),filesep)
   saveFolder(end+1) = filesep; 
end
figs = evalin('caller', 'findobj(''Type'',''figure'');');
if isempty(figs)
    success = 0;
    return
end
[~, idx] = sort([figs.Number]);
allFigs = figs(idx);

for ii = 1:length(allFigs)
    if isempty(allFigs(ii).Name)
        allFigs(ii).Name = 'noName';
    end
end
assignin('caller','allFigs',allFigs)
for ii = 1:length(allFigs)
    if exist('formatO')
        expression = sprintf('saveas(allFigs(%d), ''%s%s'',''%s'')',ii,saveFolder,allFigs(ii).Name,formatO);
        evalin('caller',expression);
    end
    expression = sprintf('saveas(allFigs(%d), ''%s%s'',''%s'')',ii,saveFolder,allFigs(ii).Name,'fig');
    evalin('caller',expression);
end
success = 1;
end
