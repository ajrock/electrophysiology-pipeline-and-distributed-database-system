function data = determineStimLevels(data)
%determinStimLevels is used to determine if multiple stim levels are
%present
%   Detailed explanation goes here

% Potential stimulus parameter name lists
elecParams = {'attLeft','attRight','el_att_L','el_att_R','S1_att','S2_att'};
acouParams = {'Atten1 [dB]','Atten2 [dB]','Atten3 [dB]'};

% Find the actual names and determine active stims
elecStimInds = find(contains(data.paramNames,elecParams));
acouStimInds = find(contains(data.paramNames,acouParams));

% These bits of code find the one of two columns that is active by
% determining whether it has more than one non-fully-attenuated (120) value
% AC 2020-06-16: making new, also weak assumption that we never stimulate
% at either 100 dB SPL OR at 0 dB attenuation
elInds = elecStimInds(sum(data.stimGrid(elecStimInds,:) ~=120 & data.stimGrid(elecStimInds,:) ~=0,2)~=0);
acInds = acouStimInds(sum(data.stimGrid(acouStimInds,:) ~=120 & data.stimGrid(acouStimInds,:) ~=0,2)~=0);

% This gives us the stimulation levels used, electric in column 1, acoustic
% in column 2. If only one combination used, then final size is 1x2. If
% not, final size is nx2, where n is the number of combinations.
data.stimLevels = unique(data.stimGrid([elInds,acInds],:)','rows');
data.elInds = elInds;
data.acInds = acInds;
% Extra finessing here to ensure that eventually, unused inds are removed
data.allInds = [elecStimInds,acouStimInds];
data.unused = data.allInds(all([elecStimInds, acouStimInds] ~= [elInds, acInds ]'));


switch ~isempty(elInds) + ~isempty(acInds)*2
    case 0
        error('No active Inds found! Check parameter list!')
    case 1
        data.stimType = 'elec';
        data.leftInd = data.elInds(1);
        data.rightInd = data.elInds(2);
    case 2
        data.stimType = 'acou';
        data.leftInd = data.acInds(1);
        if length(data.acInds)==2
            data.rightInd = data.acInds(2);
        else
            data.rightInd = data.leftInd;
        end
    case 3
        if contains(data.paramNames{elInds},'L')
            data.stimType = 'bimod_elecLeft';
            data.leftInd = data.elInds;
            data.rightInd = data.acInds;
        else
            data.stimType = 'bimod_elecRight';
            data.leftInd = data.acInds;
            data.rightInd = data.elInds;
        end
end



end

