function stimType = determineStimType()
%determineHemisphere Summary of this function goes here
%   Detailed explanation goes here
validResp = 0;
while validResp == 0
   
    stimType = input('1:(e)lectric or 2:(a)coustic: ','s');
    
    if strcmp(stimType,'1')
        stimType = 'e';
    end
    if strcmp(stimType,'2')
        stimType = 'a';
    end
    
    if ~any(strcmp(stimType,{'e','a'}))
        disp('Invalid Input!')
    else
        validResp = 1;
    end
    
end

end

