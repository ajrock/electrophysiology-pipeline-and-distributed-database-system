function [ipsi,contra,bds] = detectSidedData(data,left,right,offset,offStim,rrtfFlag)
%detectSidedData generalized function shared by IO and TC functions
%(possibly evt also RRTF)
%   Detailed explanation goes here
%% Detect sided data
if sum(diff(unique(left.stim(right.stim+offset~=left.stim)))) ~= 0
    left.stimIndices = find(left.stim~=120&right.stim == offStim);
else
    left.stimIndices = [];
end
if sum(diff(unique(right.stim(right.stim+offset~=left.stim)))) ~= 0
    right.stimIndices = find(right.stim~=120&left.stim == offStim);
else
    right.stimIndices = [];
end
if offset == 0
    if ~isempty(left.stim~=offStim&right.stim~=offStim)
        bds.stimIndices = find(left.stim~=offStim&right.stim~=offStim);
    else
        bds.stimIndices = [];
    end
else
    if ~isempty(left.stim~=offStim&right.stim~=offStim)
        bds.stimIndices = find(left.stim~=offStim&right.stim~=offStim);
    else
        bds.stimIndices = [];
    end
    right.stim(bds.stimIndices)=right.stim(bds.stimIndices)+offset;
end

if strcmp(data.hemisphere,'l')
    ipsi = left;
    contra = right;
    % Key ind tracks for TC info for whether to pull freqs from left or
    % right
    if length(data.keyInds) >2 && ~exist('rrtfFlag')
        ipsi.tcKeyInds = data.keyInds([1 2]);
        contra.tcKeyInds = data.keyInds([4 3]);
    else
        ipsi.tcKeyInds = data.keyInds(1);
        contra.tcKeyInds = data.keyInds(2);
    end
else
    ipsi = right;
    contra = left;
    if length(data.keyInds) >2 && ~exist('rrtfFlag')
        ipsi.tcKeyInds = data.keyInds([4 3]);
        contra.tcKeyInds = data.keyInds([1 2]);
    else
        ipsi.tcKeyInds = data.keyInds(2);
        contra.tcKeyInds = data.keyInds(1);
    end
%     if length(data.keyInds) == 3
%         ipsi.tcKeyInds = data.keyInds([1 3]);
%         contra.tcKeyInds = data.keyInds([1 2]);
%     elseif length(data.keyInds) == 4
%         
%     else
%         error();
%     end
end

if exist('rrtfFlag')
    ipsi.data = data.spikeMat(:,ipsi.stimIndices);
    contra.data = data.spikeMat(:,contra.stimIndices);
    bds.data = data.spikeMat(:,bds.stimIndices);
else
    ipsi.data = data.spikeMat(:,:,ipsi.stimIndices);
    contra.data = data.spikeMat(:,:,contra.stimIndices);
    bds.data = data.spikeMat(:,:,bds.stimIndices);
end

ipsi.stimGrid = data.stimGrid(:,ipsi.stimIndices);
contra.stimGrid = data.stimGrid(:,contra.stimIndices);
bds.stimGrid = data.stimGrid(:,bds.stimIndices);


end

