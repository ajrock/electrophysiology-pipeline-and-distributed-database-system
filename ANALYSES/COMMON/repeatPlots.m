function [figHandle] = repeatPlots(func,contra,ipsi,bds,varargin)%figHandle,x,y,z)
%REPEATPLOTS wrapper function to perform multiple calls to a particular
%plot type, since it is often neccesary to plot bds, contra, ipsi data
%together.
% Inputs:
%   func: function to repeat, as anonymous function
%   contra/ipsi/bds: relative data structures
%   x/y/z: relevant field names to pass to func

doThese = {'contra','ipsi','bds'};

figHandle = figure('Name',func2str(func));

for ii = 1:3
    nextPlot = subplot(1,3,ii);
    doThis = eval(doThese{ii});
    if ~isempty(doThis.stimIndices)
        for jj = 1:length(varargin)
            if isfield(doThis,varargin{jj})
                argout{jj} = doThis.(varargin{jj});
            else
                argout{jj} = [];
            end
        end
        try
        nextPlot = func(nextPlot,argout{:});
        catch
        end
        title(doThese{ii})
    end
end


end

