function hemisphere = determineHemisphere(fileName)
%determineHemisphere Summary of this function goes here
%   Detailed explanation goes here
validResp = 0;
while validResp == 0
   
    hemisphere = input('1: (l)eft or 2: (r)ight hemisphere: ','s');
    
    switch hemisphere
        case {'l','1'} % left (l) or 1
            hemisphere = 'l';
            validResp = 1;
        case {'r','2'}
            hemisphere = 'r';
            validResp = 1;
        otherwise
            disp('Invalid Input!')
    end
    
end

end

