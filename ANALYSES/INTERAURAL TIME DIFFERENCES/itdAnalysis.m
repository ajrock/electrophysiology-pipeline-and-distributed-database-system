function status = itdAnalysis(data)
%itdAnalysis Summary of this function goes here
%   Detailed explanation goes here
% NOTE: this analysis does NOT flip based on ITD!
%% DISREGARD PREVIOUS NOTE
% Explicit request from Maike Vollmer.
% As of 2020-06-25, all further data IS FLIPPED to change the left-right
% organization to ipsi-contra

%% Fix stimulus parameters

data.hemisphere = determineHemisphere(data.fileName);

[data.stimMux, data.stimLevel] = determineMuxType(data.hemisphere);

data.spkCount = cellfun(@numel,data.spikeMat );

data = determineStimLevels(data);

data = reduceData(data,1);

data = spcSwpExtr(data);

% FLIPPING TO IPSI-CONTRA HERE!
if strcmp(data.hemisphere,'r')
    data.spkCount = flip(data.spkCount,3);
end

data.itds = data.stimGrid(data.keyInds,:);

if max(abs(data.itds)) < 40
    data.itds = data.itds * 1000;
end

%% Analysis

data.spkCountMean = squeeze(mean(sum(data.spkCount,1),2));
data.spkCountStd = squeeze(std(sum(data.spkCount,1),[],2));

figure('Name','RawItdFunction'); plotshaded(data.itds, [data.spkCountMean-data.spkCountStd...
    data.spkCountMean+data.spkCountStd]' ,'k')
hold on
plot(data.itds, data.spkCountMean, 'k')
legend({'STD','Mean'})
xlabel('Ipsi    --     ITD [µs]    --    Contra');
ylabel('Spike Count / rep');

data.allPulsesFit = pulseSelection(data.spkCount,data.itds,0);
if data.allPulsesFit.pVal>0.05
    fprintf('No ITD sensitivity in regular response! Skipping and doing pooled fit.\n')
end
if rem(length(data.itds),2)==1 %odd number of responses
    midPoint = round(length(data.itds)/2);
    if rem(midPoint,2)==1
        catArray = cat(3,data.spkCount(:,:,2:2:midPoint),...
            data.spkCount(:,:,midPoint),data.spkCount(:,:,midPoint+1:2:end));
    else
        catArray = cat(3,data.spkCount(:,:,2:2:midPoint),...
            data.spkCount(:,:,midPoint+1:2:end)); 
    end
else
    catArray = data.spkCount(:,:,2:2:end);
end

% Only including Pooling of data for now
pooledCount = cat(2,data.spkCount(:,:,1:2:end),catArray);
pooledItds = data.itds(1:2:end);
data.allPulsesPooledFit = pulseSelection(pooledCount,pooledItds,1);
if data.allPulsesPooledFit.pVal>0.05
    fprintf('No ITD sensitivity in pooled response! Skipping.\n')
end
%% Save and mark status
    cont = input('Continue? Y/N: ','s');
    if any(regexpi(cont,'n'))
        status = 0;
    else
        status = 1;
    end
    
    save([data.folder '\' data.fileName],'data')
    writeData(data);
    

end

function writeData(data)
% Barf the data into an excel because this is easier than teaching people
% to code. 

% using function that unpacks nested structures into a cellarray of tables.
% Will in/exclude data from here
% Keep: 1, 3,4,5,7,8,9,12,13,15,16,17,19,20,21,24
% Modify: 2,14,25
% delete: 6,10,11,18,22,23
cellTabs = unpackStruct2table(data);
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}(:,[7,8,9,13:end])= [];
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')} = ...
    [cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}...
    cell2table({input('Give a Comment: ','s')},...
    'VariableNames',{'Comment'})];
ii = 1;
while ii <= length(cellTabs)
    if numel(cellTabs{ii}) == 1
        cellTabs(ii) = [];
    else
        cellTabs{ii}.('Filename') = data.fileName;
        cellTabs{ii} = movevars(cellTabs{ii},'Filename','Before','TableTitle');
        ii = ii+1;
    end
end

newFold = fullfile(data.folder,data.fileName);
if ~exist(newFold,'dir')
    mkdir(newFold)
end

for ii = 1:numel(cellTabs)
    if exist([fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'],'file')
        delete([fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'])
    end
    writetable(cellTabs{ii},[fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'])
end


end

function [dPrime] = primifier(ITDS, avg, vari)
% This function computes d' values. Output is a square matrix of D' values
% and a figure showing so.

dPrime = zeros(length(ITDS));

for i = 1:length(ITDS) % calculated for all itds and not only range as before. 
    for j = 1:length(ITDS) % calculate every itd against all other itds
        dPrime(i,j) = abs(avg(i)-avg(j))/...
              sqrt((vari(i)^2 + vari(j)^2)/2);
    end
end

end

function data = ndtAnalysis(spkCount,itds,FIT,isPooled)
if isPooled
    poolStr = ' Pooled';
else
    poolStr = '';
end
data.spkCount = spkCount;
data.itds = itds;

spkCountMean = squeeze(mean(sum(spkCount,1),2));
spkCountVar = squeeze(var(sum(spkCount,1),[],2));

% Classic method: fit splines to data
[splineFit, ITDS] = splineConst(spkCountMean,spkCountVar,itds,poolStr);

% Poisson binomial distribution of fit data
if ~strcmp(FIT.type,'unclassified')
    binMean = FIT.pfit(ITDS)/size(spkCount,1);
    % Need to normalize if larger resp than one
    if max(binMean) >1
    binMean = binMean-min(binMean);
    binMean = binMean/max(binMean);
    end
    binVar = (1-binMean).*binMean;
    binPrime = primifier(ITDS,binMean,binVar);
else
    pI = squeeze(mean(spkCount,2));
    binMean = squeeze(sum(pI,1));
    if numel(binMean) ==1 %Single pulse exception
        binMean = pI;
        binVar = abs((1-pI).*pI);
    else
        binMean(binMean>1)=1;
        binVar= squeeze(sum((1-pI).*pI,1));
    end
    binPrime = primifier(itds,binMean,binVar);
end

classicPrime = primifier(ITDS,splineFit.meanUpsamp,splineFit.varUpsamp);

[data.classicNDT,ndtFig] = JND(ITDS,classicPrime,FIT);
ndtFig.Name = ['Normal JNDs' poolStr];
[data.binNDT, binNdtFig] = JND(ITDS,binPrime,FIT);
binNdtFig.Name = ['Binomial JNDs' poolStr];
end

function [absNdt,rawNdt] = computeJND(itds, dPrime,trig)
deltaItdMat = repmat(itds,length(itds),1)-itds';
deltaItdMat(dPrime<trig) = NaN;

[absNdt,i2] = min(abs(deltaItdMat));
ind = sub2ind([401,401],i2,1:401);
rawNdt = deltaItdMat(ind);

end

function [jnd,jndFig] = JND(ITDS, dPrime, FIT)
% Calculates actual JND values and forms them into a structure. Performs at
% 1, 1.5, and 2 trigger levels. Makes use of external function compute_JND
% to do so. Needs the itd sampling, D' matrix, and shape fitting results

[jnd.one.raw,jnd.one.notAbs] = computeJND(ITDS, dPrime, 1);
[jnd.two.raw,jnd.two.notAbs] = computeJND(ITDS, dPrime, 2);
[jnd.oneandhalf.raw,jnd.oneandhalf.notAbs] = computeJND(ITDS, dPrime, 1.5);

%% Interpolations for JND functions for certain levels

%JND @ ITD = 0
jnd.one.itd0 = interp1(ITDS,abs(jnd.one.raw),0);
jnd.two.itd0 = interp1(ITDS,abs(jnd.two.raw),0);
jnd.oneandhalf.itd0 = interp1(ITDS,abs(jnd.oneandhalf.raw),0);

% Peak ITD JNDs (not defined in sigmoids or unclassified)
if strcmp(FIT.type, 'sigmoid') == 1 || strcmp(FIT.type, 'unclassified') == 1
    jnd.one.peak = NaN;
    jnd.two.peak = NaN;
    jnd.oneandhalf.peak = NaN;
else
    jnd.one.peak        = interp1(ITDS,abs(jnd.one.raw),       FIT.itdBest);
    jnd.two.peak        = interp1(ITDS,abs(jnd.two.raw),       FIT.itdBest);
    jnd.oneandhalf.peak = interp1(ITDS,abs(jnd.oneandhalf.raw),FIT.itdBest);
end

%JND at max slope (highest sensitivity)
if strcmp(FIT.type, 'unclassified') == 1
    jnd.one.maxslope = NaN;
    jnd.two.maxslope = NaN;
    jnd.oneandhalf.maxslope = NaN;
else
    jnd.one.maxslope        = interp1(ITDS,abs(jnd.one.raw),       FIT.maxSlope);
    jnd.two.maxslope        = interp1(ITDS,abs(jnd.two.raw),       FIT.maxSlope);
    jnd.oneandhalf.maxslope = interp1(ITDS,abs(jnd.oneandhalf.raw),FIT.maxSlope);
end

%Minimum JND and it's position
[jnd.one.min, index] = min(abs(jnd.one.raw));
jnd.one.minPos = ITDS(index);
[jnd.oneandhalf.min, index] = min(abs(jnd.oneandhalf.raw));
jnd.oneandhalf.minPos = ITDS(index);
[jnd.two.min, index] = min(abs(jnd.two.raw));
jnd.two.minPos = ITDS(index);

%% Plot resulting JND curves

% D' of 1
jndFig = figure();
subplot(1,3,1);
hold on
title(['D''', ' = 1 '])
ylabel('Just Noticeable Difference [µs]');
xlabel('Ipsi -- Reference ITD -- Contra')
plot(ITDS, abs(jnd.one.raw),   'k'); 
plot(ITDS(jnd.one.raw>0), abs(jnd.one.raw(jnd.one.raw>0)),   'k.');
plot(ITDS(jnd.one.raw<0), abs(jnd.one.raw(jnd.one.raw<0)),   'r.'); 
plot(0, jnd.one.itd0,'bx', 'MarkerSize', 10)
legStr = {'JND interp','JND points','ITD0'};
if strcmp(FIT.type, 'unclassified') == 1
    % plot neither maxslope nor Best ITD
elseif strcmp(FIT.type, 'sigmoid') == 1
    % Plot only maxslope
    plot(FIT.maxSlope, jnd.one.maxslope,'b*', 'MarkerSize', 10)
    legStr= [legStr, {'MS'}];
else
    % Plot both max slope and Best
    plot(FIT.maxSlope, jnd.one.maxslope,'b*', 'MarkerSize', 10)
    plot(FIT.itdBest, jnd.one.peak, 'bo', 'MarkerSize', 10)
    legStr= [legStr, {'MS','ITD best'}];
end
% Plot minimum JND for all
plot(jnd.one.minPos, jnd.one.min, 'bd', 'MarkerSize', 10)
legStr= [legStr, {'Min JND'}];
hold off
xlim([ITDS(1) ITDS(end)])
legend(legStr);

% D' of 1.5
subplot(1,3,2);
hold on
title(['D''', ' = 1.5 '])
ylabel('Just Noticeable Difference [µs]');
plot(ITDS, abs(jnd.oneandhalf.raw),   'k'); 
plot(ITDS(jnd.oneandhalf.raw>0), abs(jnd.oneandhalf.raw(jnd.oneandhalf.raw>0)),   'k.');
plot(ITDS(jnd.oneandhalf.raw<0), abs(jnd.oneandhalf.raw(jnd.oneandhalf.raw<0)),   'r.'); 
plot(0, jnd.oneandhalf.itd0,'bx', 'MarkerSize', 10)
if strcmp(FIT.type, 'unclassified') == 1
    % plot neither maxslope nor Best ITD
elseif strcmp(FIT.type, 'sigmoid') == 1
    % Plot only maxslope
    plot(FIT.maxSlope, jnd.oneandhalf.maxslope,'b*', 'MarkerSize', 10);
else
    % Plot both max slope and Best
    plot(FIT.maxSlope, jnd.oneandhalf.maxslope,'b*', 'MarkerSize', 10);
    plot(FIT.itdBest, jnd.oneandhalf.peak, 'bo', 'MarkerSize', 10)
end
% Plot minimum JND for all
plot(jnd.oneandhalf.minPos, jnd.oneandhalf.min, 'bd', 'MarkerSize', 10)
hold off
xlim([ITDS(1) ITDS(end)])

% D' of 2
subplot(1,3,3);
hold on
title(['D''', ' = 2 '])
ylabel('Just Noticeable Difference [µs]');
plot(ITDS, abs(jnd.two.raw),   'k'); 
plot(ITDS(jnd.two.raw>0), abs(jnd.two.raw(jnd.two.raw>0)),   'k.');
plot(ITDS(jnd.two.raw<0), abs(jnd.two.raw(jnd.two.raw<0)),   'r.'); 
plot(0, jnd.two.itd0,'bx', 'MarkerSize', 10)
if strcmp(FIT.type, 'unclassified') == 1
    % plot neither maxslope nor Best ITD
elseif strcmp(FIT.type, 'sigmoid') == 1
    % Plot only maxslope
    plot(FIT.maxSlope, jnd.two.maxslope,'b*', 'MarkerSize', 10);
else
    % Plot both max slope and Best
    plot(FIT.maxSlope, jnd.two.maxslope,'b*', 'MarkerSize', 10);
    plot(FIT.itdBest, jnd.two.peak, 'bo', 'MarkerSize', 10)
end
% Plot minimum JND for all
plot(jnd.two.minPos, jnd.two.min, 'bd', 'MarkerSize', 10)
hold off
xlim([ITDS(1) ITDS(end)])
end

function [Spline, ITDS] = splineConst(spkCount,spkVar,itds,poolStr)
%Grouping function meant to fit splines and create figures of this process.
%Uses universal sub function createFit. Takes in ITD struct containing itd
%values and their response properties, and outputs a struct containing all
%spline data, as well as the upsample rate

% Fit mean to spline
[Spline.meanfit, ~, ~, splineMeanFig] = createFit(itds', spkCount); 
splineMeanFig.Name = ['SplineFitToMean' poolStr];
h = gca;
legend(h, 'Mean SpikeCount vs. itds', 'Cubic Spline Fit', 'Location', 'NorthEast' );
% Label axes
xlabel( 'Ipsi     --     ITD     --      Contra' );
ylabel( 'Mean Spike Count' );
% Fit variance to spline (same as mean)
[Spline.varfit, ~, ~, splineVarFig] = createFit(itds', spkVar);
splineVarFig.Name = ['SplineFitToVariance' poolStr];
h = gca;
legend(h, 'Variance of SpikeCount vs. itds', 'Cubic Spline Fit', 'Location', 'NorthEast' );
% Label axes
xlabel( 'Ipsi     --     ITD     --     Contra' );
ylabel( 'Variance of Spike Count' );
% Perform Upsampling
ITDS = itds(1):abs(itds(1)-itds(end))/400:itds(end);
Spline.meanUpsamp = Spline.meanfit(ITDS);
Spline.varUpsamp = Spline.varfit(ITDS);

end

function data = pulseSelection(spkCount, itds,isPooled)

data.spkCount = spkCount;
data.itds = itds;
data.itdStepSize = abs(itds(1)-itds(2));

% STVR
[~, data.anovaTable] = anova1(squeeze(sum(spkCount,1)), [], 'off');

data.stvr = data.anovaTable{2,2} / data.anovaTable{4,2};
data.adjStvr = (data.anovaTable{2,2} - data.anovaTable{2,3} * data.anovaTable{3,4}) / (data.anovaTable{4,2} + data.anovaTable{3,4});

limit = min(size(data.spkCount,2), 12);
[~, data.limitedAnovaTable] = anova1(squeeze(sum(data.spkCount(:,1:limit,:),1)), [], 'off');
data.limitedStvr = data.limitedAnovaTable{2,2} / data.limitedAnovaTable{4,2};
data.limitedAdjStvr = (data.limitedAnovaTable{2,2} - data.limitedAnovaTable{2,3} * data.limitedAnovaTable{3,4}) / (data.limitedAnovaTable{4,2} + data.limitedAnovaTable{3,4});
data.spkCountMean = squeeze(mean(sum(data.spkCount,1),2));
data.pVal = data.limitedAnovaTable{2,6};
% Shape
if data.limitedAnovaTable{2,6} <= 0.05
    data = fitting(data,isPooled);
    data.ndt = ndtAnalysis(data.spkCount,data.itds,data.FIT,isPooled);
else
    data.FIT.type= 'unclassified';
end

end

function data = fitting(data,isPooled)
if isPooled
    poolStr = ' Pooled';
else
    poolStr = '';
end
itds = round(data.itds);
m_spkcount = data.spkCountMean;
[gof, pfit] = sigGaussFit(itds,m_spkcount);
vals = coeffvalues(pfit);
upSamp = itds(1):1:itds(end);
y = vals(1)*2.^(-((upSamp-vals(2))/(vals(3)/2)).^2)+vals(4)./(1+3.^(-2.*(upSamp-vals(2))/vals(3)))+vals(5);
fit_fig = figure('Name',['ResultingFit' poolStr]);
legend('off')
plot(itds, m_spkcount,'Color','k','LineWidth',2); %dummy plot for range used
hold on
plot(upSamp, y,'r--'); % pfit is switched out
xlabel('Ipsi     --     ITD [µs]     --      Contra');
ylabel('spikecount [spks/rep]');
legend({'Raw Spikes/rep','Current Fit'})
hold off

% Initial classification guesses
if abs(vals(1))>abs(vals(4)) % is 'gauss' term bigger than 'sigmoid'?
    % Peak or trough
    if vals(1) < 0 %trough
        type = 'trough';
    else %peak
        type = 'peak';
    end
else
    type = 'sigmoid';
end

ok =0;
r = gof.rsquare;

fprintf('r^2 value equal to %g \n', r)
fprintf('First guess at classification: %s \n', type)
while ok~=1
% User input.
if exist('resp') && resp == 4
    disp('Cancelled DOG detected, reverting to restricted range')
    resp = 2;
else
    fprintf(['Choose an Option: \n'...
        '1: continue with this fit \n'...
        '2: reduce itd range for fit \n'...
        '3: change the classification \n'...
        '4: try a DOG fit \n'...
        '5: continue with fit designated as unclassified \n'...
        '6: continue with fit designated as flat \n'])
    resp = input(' ');
    if isempty(resp) | ~isnumeric(resp) | resp < 1 | resp > 6
        warndlg('Numbers between 1 and 5 only you pitiful excuse of a human being!')
        resp = 2;
    end
end

    switch resp
        case 1
            if ~exist('itdIndRange','var')
                itdIndRange = 1:length(itds);
            end
            upSamp = itds(1):1:itds(end);
            y = vals(1)*2.^(-((upSamp-vals(2))/(vals(3)/2)).^2)+vals(4)./(1+3.^(-2.*(upSamp-vals(2))/vals(3)))+vals(5);
            close(fit_fig)
            fit_fig = figure('Name',['ResultingFit' poolStr]);
            hold on
            plot(itds(itdIndRange), m_spkcount(itdIndRange),'Color','k','LineWidth',2);
            plot(itds, m_spkcount);
            plot(upSamp, y,'r--');
            xlabel('Ipsi     --     ITD [µs]     --     Contra');
            ylabel('spikecount [spks/rep]');
            legend('RestrictRange','RawDat','Fit')
            hold off
            FIT = param(pfit, type, fit_fig, gof,itds);
            FIT.start = itds(1);
            FIT.stop = itds(end);
            %legend('RawDat','Fit')
            ok = input('Is Fit still OK? [1:yes; 2:no] ');
        case 2
            FIT.start = input('Lowest ITD for fit? ');
            FIT.stop  = input('Highest ITD for fit? ');
            if FIT.start > FIT.stop
                warndlg('ITD range not possible.')
                continue
            end
            keepFig = input('Keep last figure? 1 = yes ');
            if keepFig ~= 1
                close(fit_fig)
            end

            itdIndRange = find(itds <= FIT.stop & itds >= FIT.start);
            try
                [gof, pfit] = sigGaussFit(itds(itdIndRange),m_spkcount(itdIndRange));
                vals = coeffvalues(pfit);
            catch
                fprintf('Not enough data points!')
                [gof, pfit] = sigGaussFit(itds,m_spkcount);
                vals = coeffvalues(pfit);
            end
            
            %reinit figure
            upSamp = itds(1):1:itds(end);
            y = vals(1)*2.^(-((upSamp-vals(2))/(vals(3)/2)).^2)+vals(4)./(1+3.^(-2.*(upSamp-vals(2))/vals(3)))+vals(5);
            fit_fig = figure('Name',['ResultingFit' poolStr]);
            hold on
            plot(itds(itdIndRange), m_spkcount(itdIndRange),'Color','k','LineWidth',2);
            plot(itds, m_spkcount);
            plot(upSamp, y,'r--');
            xlabel('Ipsi     --     ITD [µs]     --     Contra');
            ylabel('spikecount [spks/rep]');
            legend('RestrictRange','Fit','RawDat')
            hold off
            
            % Initial classification guesses
            
            if abs(vals(1))>abs(vals(4)) % is 'gauss' term bigger than 'sigmoid'?
                % Peak or trough
                if vals(1) < 0 %trough
                    type = 'trough';
                else %peak
                    type = 'peak';
                end
            else
                type = 'sigmoid';
            end
            r = gof.rsquare;
            fprintf('r^2 value equal to %g \n', r)
            fprintf('First guess at classification: %s \n', type)
        case 3
            if ~exist('itdIndRange','var')
                itdIndRange = 1:length(itds);
            end
            close(fit_fig)
            fit_fig = figure('Name',['ResultingFit' poolStr]);
            hold on
            upSamp = itds(1):1:itds(end);
            y = vals(1)*2.^(-((upSamp-vals(2))/(vals(3)/2)).^2)+vals(4)./(1+3.^(-2.*(upSamp-vals(2))/vals(3)))+vals(5);
            plot(itds(itdIndRange), m_spkcount(itdIndRange),'Color','k','LineWidth',2);
            plot(itds, m_spkcount);
            plot(upSamp, y,'r--');
            xlabel('Ipsi     --     ITD [µs]     --     Contra');
            ylabel('spikecount [spks/rep]');
            legend('RestrictRange','Fit','RawDat')
            hold off
            fprintf(['Choose a Type: \n'...
                '1: peak \n'...
                '2: trough \n'...
                '3: sigmoid \n'])
            resp = input(' ');
            switch resp
                case 1
                    type = 'peak';
                case 2
                    type = 'trough';
                case 3
                    type = 'sigmoid';
            end
            FIT = param(pfit, type, fit_fig, gof,itds);
            FIT.start = itds(1);
            FIT.stop = itds(end);
            ok = input('Is Fit still OK? [1:yes; 2:no] ');
        case 4
            % DOG Fit
            type = 'DOG';
            [gof, pfit, fit_fig] = dogFit(itds,m_spkcount, fit_fig, poolStr);
            r = gof.rsquare;
            fprintf('r^2 value equal to %g \n', r)
            ok = input('Is Fit OK? [1:yes; 2:no] ');
            if ok == 1
                FIT = param(pfit, type, fit_fig, gof,itds);
                FIT.start = itds(1);
                FIT.stop = itds(end);
                ok = input('Is Fit still OK? [1:yes; 2:no] ');
            end
        case 5
            FIT.type = 'unclassified';
            [FIT.maxSpk, FIT.maxSpkInd] = max(m_spkcount);
            [FIT.minSpk, FIT.minSpkInd] = min(m_spkcount);
            FIT.modDepthRaw =  (FIT.maxSpk-FIT.minSpk)/FIT.maxSpk;
            ok = 1;
        case 6
            FIT.type = 'Flat';
            ok = 1;
        otherwise
            fprintf('Invalid input.')
    end
    
end
data.FIT = FIT;

end

function FIT = param(pfit,type,fit_fig,gof,itds)

% obvious things first
FIT.pfit=pfit;
FIT.gof=gof;
FIT.type = type;
fitAx = gca(fit_fig);
origLegend = legend;
origLegend = origLegend.String;
% Get fit approximation for parameters
x = itds(1):0.01:itds(end);
y = pfit(x);

switch type
    case {'peak','trough'}
        if strcmp(type,'peak')
            [FIT.spikePeak, itdBestInd] = max(y);
        else
            [FIT.spikePeak, itdBestInd] = min(y);
        end
        FIT.itdBest = x(itdBestInd);
        
        
        if FIT.itdBest > 0
            posFind = @(itdBestInd,y,z) min(abs(y(1:itdBestInd)-z));
            [~, maxSlopeInd] = max(abs(diff(y(1:itdBestInd))));
            FIT.maxSlope = x(maxSlopeInd);
            if strcmp(type,'peak')
                rateChange = FIT.spikePeak - min(y(1:itdBestInd));
            else
                rateChange = max(y(1:itdBestInd)) - FIT.spikePeak ;
            end
            rate25 = min(y(1:itdBestInd)) + rateChange*0.25;
            rate50 = min(y(1:itdBestInd)) + rateChange*0.5;
            rate75 = min(y(1:itdBestInd)) + rateChange*0.75;
            [~, x25] = posFind(itdBestInd,y,rate25);
            [~, x50] = posFind(itdBestInd,y,rate50);
            [~, x75] = posFind(itdBestInd,y,rate75);
            FIT.halfrise = x(x75)-x(x25);
            FIT.halfwidth = FIT.itdBest-x(x50);
        else
            posFind = @(itdBestInd,y,z) min(abs(y(itdBestInd:end)-z));
            [~, maxSlopeInd] = max(abs(diff(y(itdBestInd:end))));
            FIT.maxSlope = x(maxSlopeInd+itdBestInd);
            if strcmp(type,'peak')
                rateChange = FIT.spikePeak - min(y(itdBestInd:end));
            else
                rateChange = max(y(itdBestInd:end)) - FIT.spikePeak;
            end
            rate25 = min(y(itdBestInd:end)) + rateChange*0.25;
            rate50 = min(y(itdBestInd:end)) + rateChange*0.5;
            rate75 = min(y(itdBestInd:end)) + rateChange*0.75;
            [~, x25] = posFind(itdBestInd,y,rate25);
            [~, x50] = posFind(itdBestInd,y,rate50);
            [~, x75] = posFind(itdBestInd,y,rate75);
            x25 = x25+itdBestInd;
            x75 = x75+itdBestInd;
            FIT.halfrise = x(x25)-x(x75);
            FIT.halfwidth = x(x50+itdBestInd)-FIT.itdBest;
        end
        
        legStr = [origLegend {'Best','MS','25%','75%'}];
        hold on
        scatter(fitAx,FIT.itdBest,FIT.spikePeak,'d','filled','MarkerFaceColor',[0 1 1])
        hold off
        
    case 'sigmoid'
        FIT.itdBest = NaN;
        FIT.spikePeak = NaN;
        [~, maxSlopeInd] = max(abs(diff(y)));
        FIT.maxSlope = x(maxSlopeInd);
        [~, bInd] = min(abs(x-pfit.b));
        [maxVal,maxInd] = max(y);
        [minVal,minInd] = min(y);
        rateChange = maxVal - minVal;
        rate25 = minVal + rateChange*0.25;
        rate75 = minVal + rateChange*0.75;
        searchRange = y(min(minInd,maxInd):max(minInd,maxInd));
        [~, x25] = min(abs(searchRange-rate25));
        x25 = x25 + min(minInd,maxInd);
        [~, x75] = min(abs(searchRange-rate75));
        x75 = x75 + min(minInd,maxInd);
        FIT.halfrise = abs(x(x25)-x(x75));
        
        legStr = [origLegend {'MS','25%','75%'}];
    otherwise
        [FIT.spikePeak, itdBestInd] = max(y);
        FIT.itdBest = x(itdBestInd);
        hold on
        scatter(fitAx,FIT.itdBest,FIT.spikePeak,'d','filled','MarkerFaceColor',[0 1 1])
        hold off
        [miny, minyInd] = min(y);
        FIT.maxSlope = x(round((minyInd+itdBestInd)/2));
        rateChange = FIT.spikePeak-miny;
        rate25 = rateChange*0.25 + miny;
        rate75 = rateChange*0.75 + miny;
        searchRange = y(min(minyInd,itdBestInd):max(minyInd,itdBestInd));
        [~, x25] = min(abs(searchRange-rate25));
        x25 = x25 + min(minyInd,itdBestInd);
        [~, x75] = min(abs(searchRange-rate75));
        x75 = x75 + min(minyInd,itdBestInd);
        FIT.halfrise = abs(x(x75)-x(x25));
        %[~,maxSlopeInd] = posFind(itdBestInd,miny,minyInd,FIT.maxSlope);
        legStr = [origLegend {'Best','MS','25%','75%'}];
end
FIT.x25 = x(x25);
FIT.x75 = x(x75);

% Adding code to track all major slope maxima
slopes = diff(y);
% Sorts peaks by highest amplitude
[pks,locs]=findpeaks(abs(slopes),'SortStr','descend');
switch type
    case {'peak','trough'}
        keep = 2;
        legStr = [legStr {'Slope 1', 'Slope 2'}];
    case 'sigmoid'
        keep = 0;
    otherwise
        keep = 3;
        legStr = [legStr {'Slope 1', 'Slope 2','Slope 3'}];
end
FIT.sizeSortedSlopeMaxima = x(locs(1:min(keep,length(locs))));

hold on
scatter(fitAx,FIT.maxSlope, FIT.pfit(FIT.maxSlope),90,'filled','MarkerFaceColor',[0.8500 0.3250 0.0980])
scatter(fitAx,FIT.x25,y(x25),'filled','MarkerFaceColor',[0.9290 0.6940 0.1250])
scatter(fitAx,FIT.x75,y(x75),'filled','MarkerFaceColor',[0.4940 0.1840 0.5560])
for ii = 1:min(keep,length(locs))
    scatter(fitAx,FIT.sizeSortedSlopeMaxima(ii),y(locs(ii)),60,'*','MarkerEdgeColor',[1 1 1]*0.1*ii)
end
hold off
legend(legStr)

end

function [vals, pfit, fit_fig] = dogFit(itds,spikes,fit_fig,poolStr)
% This function performs all alternate DOG fitting in the same form as
% Yoojin's 2016 paper but adapted to DOG fits

FIT.start = input('Lowest ITD for fit? ');
FIT.stop  = input('Highest ITD for fit? ');

if FIT.start > FIT.stop
    warndlg('You Stupid asshole! These values are impossible! Resetting to full range.')
    FIT.start = itds(1);
    FIT.stop = itds(end);
end

itdIndRange = find(itds <= FIT.stop & itds >= FIT.start);

if length(itdIndRange) <= 2
    warndlg('Invalid itd range ya hose head! Reverting to full range!')
    itdIndRange = find(itds <= 2000 & itds >= -2000);
end

phy = 120; %phys range

if find(spikes==max(spikes),1) < round(length(itds)/2) %indicative of pos first dog
    a = -2;%
elseif find(spikes==max(spikes),1) > round(length(itds)/2) %neg first
    a = 2;
else %probably wont happen but whatever
    a = 0;
end

% b based on estimated mid-point of the two bells
b = (itds(find(spikes==max(spikes),1)) + itds(find(spikes==min(spikes),1)))/2;

c = 500;%phy; % half-width measure

d = spikes(end)-spikes(1); % sigmoid variation

e = min(spikes); % DC offset

fity = fittype('a*( exp(-(((x-b-c)/c)^2))-exp(-(((x-b+c)/c)^2)))+d/(1+3^(-2*(x-b)/c))+e');

%% Display estimates

disp('Here is the starting guess')
y = a.*( exp(-(((itds-b-c)./c).^2))-exp(-(((itds-b+c)./c).^2)))+d./(1+3.^(-2.*(itds-b)./c))+e;

temp = figure; plot(itds,spikes)
hold on
plot(itds, y)

resp = input('Are you ok with this initial guess? Y: 1: ');

if resp ~= 1
    prompt = {...
        'Best ITD size and orientation (0 for sigmoid, + for peak, - for trough)',...
        'Best ITD location (in us, + or -)',...
        'Halfwidth (measure from Best ITD location to bell end)',...
        'Sigmoid size (change in rate from low to high response)',...
        'DC offset',...
        };
    dlg_title = 'Fit Parameters';
    num_lines = 1;
    conv = num2str([a; b; c; d; e]);
    defaultans = cellstr(conv);
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    a = str2num(answer{1});
    b = str2num(answer{2});
    c = str2num(answer{3});
    d = str2num(answer{4});
    e = str2num(answer{5});
end
close(temp)
fito = fitoptions(fity);
fito.Lower = [-10 -2000 0 -10 min(spikes)-0.1];
fito.Upper = [50 2000 4000 50 50];
fito.StartPoint = [a b c d e];
fito.MaxFunEvals = 10000;
fito.TolFun = 1e-20;
fito.TolX = 1e-20;

close(fit_fig)
clear fit_fig
[pfit, vals]=fit(itds(itdIndRange)', spikes(itdIndRange), fity, fito);

%reinit figure
fit_fig = figure('Name',['ResultingFit' poolStr]);
plot(itds, spikes);
hold on
plot(itds(itdIndRange), spikes(itdIndRange),'Color','k','LineWidth',2);
plot(pfit,'r--');
xlabel('Ipsi     --     ITD [µs]     --     Contra');
ylabel('spikecount [spks/rep]');
legend('RawDat','RestrictRange','Fit')
hold off

end

function [vals, pfit] = sigGaussFit(itds, spikes)
% This function implements the fitting procedure used by Yoojin in her 2016
% paper on rabbits. The neuron is fit to a combined gauss, sigmoid shape
% that is robust enough in its parameters to identify whether the shape is 
% gaussian, trough, or sigmoid. 

%% Estimate starting values
phy = 120; %phys range

d = mean(spikes(end-5:end))-mean(spikes(1:5)); % sigmoid variation

% 09.11.2018 AC change here to use findpeaks
[~,locs,w,p]=findpeaks(spikes,1/(itds(2)-itds(1)));
[pmax,idx]=max(p);

[~,tlocs,tw,t]=findpeaks(spikes*-1,1/(itds(2)-itds(1)));
[tmax,idt]=max(t);

if isempty(tmax) || pmax > tmax % Indicates most prominent signal was a peak, not trough
    a = pmax;
    b = itds(round(locs(idx)/(itds(2)-itds(1))));
    c = w(idx);
else
    a = tmax*-1;
    b = itds(round(tlocs(idt)/(itds(2)-itds(1))));
    c = tw(idt);
end
e = mean(spikes(1:5));

%% Display estimates

disp('Here is the starting guess')

upSamp = itds(1):1:itds(end);
y = a*2.^(-((upSamp-b)/(c/2)).^2)+d./(1+3.^(-2.*(upSamp-b)/c))+e;

temp = figure; plot(itds,spikes)
hold on
plot(upSamp, y)
xlabel('Ipsi     --     ITD [µs]     --     Contra');
ylabel('Spike Count / rep');
legend({'Raw Spikes/rep','Estimated Fit'})

resp = input('Are you ok with this initial guess? Y: 1: ');

if resp ~= 1
    prompt = {...
        'Best ITD size and orientation (0 for sigmoid, + for peak, - for trough)',...
        'Best ITD location (in us, + or -)',...
        'Halfwidth (measure from Best ITD location to bell end)',...
        'Sigmoid size (change in rate from low to high response)',...
        'DC offset',...
        };
    dlg_title = 'Fit Parameters';
    num_lines = 1;
    conv = num2str([a; b; c; d; e]);
    defaultans = cellstr(conv);
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    a = str2num(answer{1});
    b = str2num(answer{2});
    c = str2num(answer{3});
    d = str2num(answer{4});
    e = str2num(answer{5});
end
close(temp)

fity = fittype('a*2^(-((x-b)/(c/2))^2)+d/(1+3^(-2*(x-b)/c))+e');
fito = fitoptions(fity);
fito.Lower = [-10 -20000 0 -10 min(spikes)-0.1];
fito.Upper = [10 20000 4000 10 max(spikes)];
fito.StartPoint = [a b c d e];
fito.MaxFunEvals = 10000;
fito.TolFun = 1e-20;
fito.TolX = 1e-20;

[pfit, vals]=fit(itds', spikes, fity, fito);

end

function data = spcSwpExtr(data)
% This function extracts Left/Right only stim data, as well as silent sweep
% data while removing it from the master data variable in order to analyze
% the rest of the data separately

% Take ITD row out of parameters for silent decibel checks because ITD 
% value may also equal 120
%tempPar = data.stimGrid([data.leftInd data.rightInd],:);
leftVect = data.stimGrid(data.leftInd,:);
rightVect = data.stimGrid(data.rightInd,:);
rmThese = [];

x = find(leftVect==120|rightVect==120);
uniMode = (leftVect==120)*2+(rightVect==120);

for ii = 1:length(uniMode)
    switch(uniMode(ii))
        case 1 % Left
            leftOnly.spikes = data.spkCount(:,:,ii);
            leftOnly.meanSpikes = mean(sum(data.spkCount(:,:,ii),1));
            leftOnly.stdSpikes = std(sum(data.spkCount(:,:,ii),1));
            if strcmp(data.hemisphere,'l')
                data.ipsiOnly = leftOnly;
            else
                data.contraOnly = leftOnly;
            end
            rmThese = [rmThese, ii];
        case 2 % Right
            rightOnly.spikes = data.spkCount(:,:,ii);
            rightOnly.meanSpikes = mean(sum(data.spkCount(:,:,ii),1));
            rightOnly.stdSpikes = std(sum(data.spkCount(:,:,ii),1));
            if strcmp(data.hemisphere,'r')
                data.ipsiOnly = rightOnly;
            else
                data.contraOnly = rightOnly;
            end
            rmThese = [rmThese, ii];
        case 3 % SS
            data.silentSweep.spikes = data.spkCount(:,:,ii);
            data.silentSweep.meanSpikes = mean(sum(data.spkCount(:,:,ii),1));
            data.silentSweep.stdSpikes = std(sum(data.spkCount(:,:,ii),1));
            rmThese = [rmThese, ii];
    end
end
data.spkCount(:,:,rmThese)=[]; %delete special sweeps
data.stimGrid(:,rmThese)=[]; %May remove later as is unneccessary step

end
