function data = reduceData(data,stimIndex)
%REDUCEDATA Removes unused stimgrid and response matrix entries from
%dataset
%   Detailed explanation goes here

data.stimLevels = data.stimLevels(stimIndex,:);
stimInds = data.stimGrid([data.elInds,data.acInds],:);
%data.stimInds = find(stimInds(1,:)==data.stimLevels(1)&stimInds(2,:)==data.stimLevels(2));
%data.stimGrid(data.unused,:) = [];
%data.respMat = data.fullMat{:}(:,data.stimInds);

end

