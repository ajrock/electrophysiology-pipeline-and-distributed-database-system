function [stimType,stimLevel] = determineMuxType(hemi)
%determineHemisphere Summary of this function goes here
%   Detailed explanation goes here
validResp = 0;
while validResp == 0
   
    stimType = input(['Please enter the stimulus Bimod mux.\n '...
        '1:(e)lectric,\n 2:(a)coustic,\n '...
        '3:(ae)acoustic/electric,\n 4:(ea)electric/acoustic:\n '],'s');
    
    switch stimType
        case {'1','e'}
            stimType = 'electric';
            msg = 'Please enter the LEFT attenuation level used:\n';
            stimLevel(1) = str2double(input(msg,'s'));
            msg = 'Please enter the RIGHT attenuation level used:\n';
            stimLevel(2) = str2double(input(msg,'s'));
            validResp = 1;
        case {'2','a'}
            stimType = 'acoustic';
            msg = 'Please enter the decibel level used:\n';
            stimLevel = str2double(input(msg,'s'));
            validResp = 1;
        case {'3','ae'}
            if strcmp(hemi,'l')
                stimType = 'acIpsi_elContra';
            else
                stimType = 'elIpsi_acContra';
            end
            msg = 'Please enter the acoustic decibel level used:\n';
            stimLevel(1) = str2double(input(msg,'s'));
            msg = 'Please enter the electric attenuation level used:\n';
            stimLevel(2) = str2double(input(msg,'s'));
            validResp = 1;
        case {'4','ea'}
            if strcmp(hemi,'l')
                stimType = 'elIpsi_acContra';
            else
                stimType = 'acIpsi_elContra';
            end
            stimType = 'electric_acoustic';
            msg = 'Please enter the electric attenuation level used:\n';
            stimLevel(1) = str2double(input(msg,'s'));
            msg = 'Please enter the acoustic decibel level used:\n';
            stimLevel(2) = str2double(input(msg,'s'));
            validResp = 1;
        otherwise
            disp('Invalid Input!')
    end
    
end

end
