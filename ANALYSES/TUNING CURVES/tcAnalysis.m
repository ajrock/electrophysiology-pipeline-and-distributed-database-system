function status = tcAnalysis(data)
%tcAnalysis Summary of this function goes here
%   Detailed explanation goes here

data.hemisphere = determineHemisphere(data.fileName);

offStim = 120;
offset = 0;

% For the love of god, always keep in mind that idexing of the master
% stimgrid is done via ATTEN, not db spl! Atten is not a tcKeyInd, so you
% MUST locate it here!
if sum(contains(data.paramNames,{'Atten','att'}))~=2
    left.stim = data.stimGrid(data.keyInds(1),:);
    right.stim = data.stimGrid(data.keyInds(4),:);
else
    left.stim = data.stimGrid(contains(data.paramNames,'Atten1'),:);
    right.stim = data.stimGrid(contains(data.paramNames,'Atten3'),:);
    if isempty(left.stim) && isempty(right.stim)
        left.stim = data.stimGrid(contains(data.paramNames,'att_1'),:);
        right.stim = data.stimGrid(contains(data.paramNames,'att_2'),:);
    end
end

[ipsi, contra, bds] = detectSidedData(data,left,right,offset,offStim);

%% Data Calculations and form matrices
ipsi.repCount   =  squeeze(sum(cellfun(@numel,ipsi.data ),1));
contra.repCount  =  squeeze(sum(cellfun(@numel,contra.data),1));
bds.repCount    =  squeeze(sum(cellfun(@numel,bds.data  ),1));
%func = @(x) squeeze(sum(cellfun(@numel,x),1));
%fn = 'repCount';
%[ipsi.(fn),contra.(fn),bds.(fn)] = repeatFunction(func,contra,ipsi,bds,'data');

[ipsi.levels,~,splInds] = unique(ipsi.stimGrid(ipsi.tcKeyInds(1),:));
[ipsi.freqs,~,freqInds] = unique(ipsi.stimGrid(ipsi.tcKeyInds(2),:));
if ~isvector(ipsi.repCount)
    ipsi.meanCount  = full(sparse(splInds,freqInds,mean(ipsi.repCount, 1)));
else
    ipsi.meanCount  = full(sparse(splInds,freqInds,ipsi.repCount));
end
ipsi.stdCount  = full(sparse(splInds,freqInds,std(ipsi.repCount, 1)));

[contra.levels,~,splInds] = unique(contra.stimGrid(contra.tcKeyInds(1),:));
[contra.freqs,~,freqInds] =  unique(contra.stimGrid(contra.tcKeyInds(2),:));
if ~isvector(contra.repCount)
    contra.meanCount  = full(sparse(splInds,freqInds,mean(contra.repCount, 1)));
else
    contra.meanCount  = full(sparse(splInds,freqInds,contra.repCount));
end
contra.stdCount  = full(sparse(splInds,freqInds,std(contra.repCount, 1)));

[bds.levels,~,splInds] = unique(bds.stimGrid(data.keyInds(1),:));
[bds.freqs,~,freqInds] =  unique(bds.stimGrid(data.keyInds(2),:));
if ~isvector(bds.repCount)
    bds.meanCount  = full(sparse(splInds,freqInds,mean(bds.repCount, 1)));
else
    bds.meanCount  = full(sparse(splInds,freqInds,bds.repCount));
end
bds.stdCount  = full(sparse(splInds,freqInds,std(bds.repCount, 1)));



%% Check output and decide whether to proceed
fig = repeatPlots(@tcContours,contra,ipsi,bds,'levels','freqs','meanCount');
fig.Name = 'tcContours_rawAverages';

cont = input('Continue? Y/N: ','s');

if any(regexpi(cont,'n'))
    status = 0;
    data.ipsi = ipsi;
    data.contra = contra;
    data.bds = bds;
else
    %fig=figure('Position', [10 50 560 420]);
    if ~isempty(bds.stimIndices)
        [bds.io,bds.smoothMat,bds.ibt] = BlobFinder(bds.meanCount,0,1);
        bds = parameterFinder(bds,[]);
        bds.Qfact = getQ(bds);
    end
    if ~isempty(ipsi.stimIndices)
        [ipsi.io,ipsi.smoothMat,ipsi.ibt] = BlobFinder(ipsi.meanCount,0,1);
        ipsi = parameterFinder(ipsi,[]);
        ipsi.Qfact = getQ(ipsi);
    end
    if ~isempty(contra.stimIndices)
        [contra.io,contra.smoothMat,contra.ibt] = BlobFinder(contra.meanCount,0,1);
        contra = parameterFinder(contra,[]);
        contra.Qfact = getQ(contra);
    end
    smoothFig = repeatPlots(@tcContours,contra,ipsi,bds,'levels','freqs','smoothMat');
    smoothFig.Name = 'tcContours_smoothOutput';
    ioFig = repeatPlots(@tcContours,contra,ipsi,bds,'levels','freqs','io');
    ioFig.Name = 'InOutGrid';
    bdsIo = ioFig.Children(1);
    ipsiIo = ioFig.Children(2);
    contraIo = ioFig.Children(3);
    legString = {'','CF','BF'};
    hold on;
    legFlag = 1;
    if ~isempty(bds.stimIndices)
        thisLeg = legString;
        hold(bdsIo,'on')
        scatter(bdsIo,bds.CF,bds.thr,'rx')
        scatter(bdsIo,bds.BF,bds.bfLevel,'ro')
        if isfield(bds.Qfact,'cfQrel10') && ~isnan(bds.Qfact.cfQrel10)
            plot(bdsIo,...
                [bds.Qfact.lowerFreqRel10 bds.Qfact.upperFreqRel10],...
                [bds.thr bds.thr]+10,'r--')
            thisLeg = [thisLeg 'Q@10overThr'];
        end
        hold(bdsIo,'off')
        legend(bdsIo,thisLeg)
        legFlag = 0;
    end
    if ~isempty(ipsi.stimIndices)
        thisLeg = legString;
        hold(ipsiIo,'on')
        scatter(ipsiIo,ipsi.CF,ipsi.thr,'rx')
        scatter(ipsiIo,ipsi.BF,ipsi.bfLevel,'ro')
        if isfield(ipsi.Qfact,'cfQrel10') && ~isnan(ipsi.Qfact.cfQrel10)
            plot(ipsiIo,...
                [ipsi.Qfact.lowerFreqRel10 ipsi.Qfact.upperFreqRel10],...
                [ipsi.thr ipsi.thr]+10,'r--')
            thisLeg = [thisLeg 'Q@10overThr'];
        end
        if legFlag
            legend(ipsiIo,thisLeg)
            legFlag = 0;
        end
        hold(ipsiIo,'off')
    end
    if ~isempty(contra.stimIndices)
        thisLeg = legString;
        hold(contraIo,'on')
        scatter(contraIo,contra.CF,contra.thr,'rx')
        scatter(contraIo,contra.BF,contra.bfLevel,'ro')
        if isfield(contra.Qfact,'cfQrel10') && ~isnan(contra.Qfact.cfQrel10)
            plot(contraIo,...
                [contra.Qfact.lowerFreqRel10 contra.Qfact.upperFreqRel10],...
                [contra.thr contra.thr]+10,'r--')
            thisLeg = [thisLeg 'Q@10overThr'];
        end
        hold(contraIo,'off')
        if legFlag
            legend(contraIo,thisLeg)
            legFlag = 0;
        end
    end
    
    % Aural Dominance
    if ~isempty(ipsi.stimIndices) && ~isempty(contra.stimIndices) &&...
            (sum(contra.io,'all') + sum(ipsi.io,'all')) > 0
        data.auralDom = auralDomOmniator2019(contra,ipsi);
        adFig = figure('Position', [10 50 560 420],'Name','auralDom'); 
        pcolor(data.auralDom.auralDom); colorbar
        xlabel('Freq [kHz]')
        ylabel('dB SPL')
        title('Dominance')
    end
    cont = input('Continue? Y/N: ','s');
    if any(regexpi(cont,'n'))
        status = 0;
    else
        status = 1;
    end
    
    data.bds = bds;
    data.contra = contra;
    data.ipsi = ipsi;
    data.status = status;
    
    save([data.folder '\' data.fileName],'data')
    writeData(data)
    
end

end

function writeData(data)
% Barf the data into an excel because this is easier than teaching people
% to code. 

% using function that unpacks nested structures into a cellarray of tables.
% Will in/exclude data from here
% Keep: 1, 3,4,5,7,8,9,12,13,15,16,17,19,20,21,24
% Modify: 2,14,25
% delete: 6,10,11,18,22,23
cellTabs = unpackStruct2table(data);
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}.sourceFiles = data.srcFile;
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'contra')}.contra_tcKeyInds= [];
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'ipsi')}.ipsi_tcKeyInds= [];
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')} = ...
    [cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}...
    cell2table({input('Give a Comment: ','s')},...
    'VariableNames',{'Comment'})]

ii = 1;
while ii <= length(cellTabs)
    if numel(cellTabs{ii}) == 1
        cellTabs(ii) = [];
    else
        cellTabs{ii}.('Filename') = data.fileName;
        cellTabs{ii} = movevars(cellTabs{ii},'Filename','Before','TableTitle');
        ii = ii+1;
    end
end

newFold = fullfile(data.folder,data.fileName);
if ~exist(newFold,'dir')
    mkdir(newFold)
end

for ii = 1:numel(cellTabs)
    if exist([fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'],'file')
        delete([fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'])
    end
    writetable(cellTabs{ii},[fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'])
end


end
