function data = parameterFinder(data,plot2use)
%% Extract single map Characteristics
% Threshold: lowest dB at which spikes are seen
% Best Frequency: frequency at which highest spike rate occurs
% Characteristic Frequency: frequency at which the lowest dB threshold
% is achieved
% Spontaneous Rate: number of spikes spontaneously firing, usually
% denoted in spikes/ms (keep in mind that last recorded spon rate is
% per sweep
% Failure Rate: %'age of valid responses that had sweeps fail to elicit a
% response. Taken from BF and surrounding pixels
% First Latency Jitter and Mean: variance of BF and surrounding pixels first
% recorded spike.
% Spike count Jitter: variance of BF and pixels for number of spikes
% recorded
in_out_grid = data.io;
filtered_tc_data = data.smoothMat;
thrInd = find(sum(in_out_grid,2),1); %intermediate step
if isempty(thrInd)
    data.thr = data.levels(end)+5;
else
    data.thr = data.levels(thrInd);
end
ioTcDat = filtered_tc_data.*in_out_grid;
% BF is one-lined using some indexing tricks in find
[bfLevInd, bfFreqInd] = ind2sub(size(ioTcDat),...
    find(ioTcDat==max(ioTcDat(:)),1));
data.BF  = data.freqs(bfFreqInd);
data.bfLevel = data.levels(bfLevInd);
% Careful! Shortcuts taken mean that if multiple points on IO grid drop
% to thr, CF will be incorrect!
ioCheck = find(in_out_grid(thrInd,:)==1);
data.multiPeakFlag = 0;
if sum(diff(ioCheck)) ~= length(ioCheck)-1 % Multiple IO peaks!
    data.multiPeakFlag = 1;
    % Try to separate. Hopefully there are only two, and one is wider.
    % Use the wider one.
    separator = find(diff(ioCheck)~=1);
    if length(separator) == 1 % It worked! take the correct one
        if length(ioCheck)/separator > 1 % indicates that first peak should be taken. We take the wider peak
            ioCheck = ioCheck(1:separator);
        elseif length(ioCheck)/separator < 1 % second peak
            ioCheck = ioCheck(separator:end);
        else % I really hope this doesn't happen
            error('Two equal width IO Peaks Detected!')
        end
    else %Take strongest line
        strongDat = filtered_tc_data(thrInd:end,ioCheck);
        meanStrongDat = mean(strongDat,1);
        [~,winnerIndex] = max(meanStrongDat);
        ioCheck = ioCheck(winnerIndex);
    end
end
data.CF  = mean(data.freqs(ioCheck));
%hold on;
%scatter(plot2use,data.CF,data.thr,'rx')
%scatter(plot2use,data.BF,data.bfLevel,'ro')

%% Latency & Jitter measures
data.bfLatencyStats = get3x3response(data,data.BF,data.bfLevel);
data.cfPlus15LatencyStats = get3x3response(data,data.CF,data.thr+15);
data.cfPlus20LatencyStats = get3x3response(data,data.CF,data.thr+20);

end

function results = get3x3response(data,freq,level)

[~,col] = min(abs(data.freqs-freq));
[~,row] = min(abs(data.levels-level));

rows = [row-1;row;row+1];
rows(rows<1|rows>length(data.levels))=[];
cols = [col-1, col,col+1];
cols(cols<1|cols>length(data.freqs))=[];

[I1, I2] = meshgrid(cols,rows);

latCells = numel(I1);
if any(sub2ind(size(data.io),I2(:),I1(:))>size(data.data,3),'all')
    lats = {nan};
else
    lats = squeeze(data.data(:,:,sub2ind(size(data.io'),I1(:),I2(:))));
    if any(cellfun(@isempty,lats),'all')
        lats(cellfun(@isempty,lats)) = {NaN};
    end
end

firstLats = cellfun(@(x) x(1),lats);
spkCount = sum(~cellfun(@(x) any(isnan(x)),lats)); 
failures = 1-spkCount; failures(failures<0) = 0;

results.meanFirstLat = mean(firstLats,'all','omitnan');
results.stdFirstLat = std(firstLats,[],'all','omitnan');
results.failureRate = 1-sum(failures,'all')/numel(failures);
results.spikeCountMean = mean(spkCount,'all');
results.spikeCountStd = std(spkCount,[],'all');

results.spikeCountMeanNoZeros = mean(spkCount(spkCount~=0),'all');
results.spikeCountStdNoZeros = std(spkCount(spkCount~=0),[],'all');


end











