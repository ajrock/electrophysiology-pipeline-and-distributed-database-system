function data = auralDomOmniator2019(contraDat, ipsiDat)
% This function, written by Andrew Curran 2018 as part of the AG Vollmer in
% Magdeburg, is used to compare tuning maps obtained for single unit
% neurons in Gerbil IC. Alpha Version.

% To begin, will take in two table indices (without checking them for match) and
% output the aural dominance characteristics. For example, giving values of
% 1 and 3 to the program will cause it to compare the entries found at
% positions 1 and 3 of the master TC data table.

% Future considerations may need to be taken in case of mismatched freq/SPL
% levels! I.e. contra taken 10-85, 250-8000, ipsi 40-70, 250-2000! Let's
% hope not though

% 2018.08.06 AC: Day has come to consider mismatched frequency/stim levels.
% God this is a pain...
% 2019-05-09 AC: changing input to reflect new analysis. Redo-ing IO grid
% for safety.


% Break relevant data out of cells
contraStimLevels = contraDat.levels;
ipsiStimLevels = ipsiDat.levels;
contraStimFreqs = contraDat.freqs;
ipsiStimFreqs = ipsiDat.freqs;
contraFiltered_tc_data = contraDat.smoothMat;
ipsiFiltered_tc_data = ipsiDat.smoothMat;
contraIoGrid = contraDat.io;
ipsiIoGrid = ipsiDat.io;

% Check for well matched data
if sum(size(contraFiltered_tc_data) ~= size(ipsiFiltered_tc_data)) > 0
    % Try to reduce data to common levels. Basically, we will find common
    % levels and 'squeeze' both sets through the same restrictions.
    if length(contraStimFreqs) ~= length(ipsiStimFreqs)
        % get matching values
        matches = intersect(contraStimFreqs,ipsiStimFreqs);
        % Get matching indices
        [~,contraMatchInds] = intersect(contraStimFreqs,matches);
        [~,ipsiMatchInds] = intersect(ipsiStimFreqs,matches);
        % Squeeze contra levels
        contraStimFreqs = contraStimFreqs(contraMatchInds);
        contraIoGrid = contraIoGrid(:,contraMatchInds);
        contraFiltered_tc_data = contraFiltered_tc_data(:,contraMatchInds);
        
        %Squeeze ipsi levels
        ipsiStimFreqs = ipsiStimFreqs(ipsiMatchInds);
        ipsiIoGrid = ipsiIoGrid(:,ipsiMatchInds);
        ipsiFiltered_tc_data = ipsiFiltered_tc_data(:,ipsiMatchInds);
    end
    if length(contraStimLevels) ~= length(ipsiStimLevels)
        % get matching values
        matches = intersect(contraStimLevels,ipsiStimLevels);
        % Get matching indices
        [~,contraMatchInds] = intersect(contraStimLevels,matches);
        [~,ipsiMatchInds] = intersect(ipsiStimLevels,matches);
        % Squeeze contra levels
        contraStimLevels = contraStimLevels(contraMatchInds);
        contraIoGrid = contraIoGrid(contraMatchInds,:);
        contraFiltered_tc_data = contraFiltered_tc_data(contraMatchInds,:);
        
        %Squeeze ipsi levels
        ipsiStimLevels = ipsiStimLevels(ipsiMatchInds);
        ipsiIoGrid = ipsiIoGrid(ipsiMatchInds,:);
        ipsiFiltered_tc_data = ipsiFiltered_tc_data(ipsiMatchInds,:);
    end
    %recheck
    if sum(size(contraFiltered_tc_data) ~= size(ipsiFiltered_tc_data)) > 0
        error('TCs are not well matched!')
    end
end

% Aural Dominance Calculation
% Use union of contra and ipsi IO grids. IE final matrix should have a grid
% of overlap

data.unionIO = zeros(size(contraIoGrid));
data.unionIO(contraIoGrid==1 |...
    ipsiIoGrid==1) = 1;

% Filter both TC Data sets by the unionIO

contraTC = contraFiltered_tc_data.*data.unionIO;
ipsiTC= ipsiFiltered_tc_data.*data.unionIO;

% Aural dominance is defined as C - I / C + I
data.auralDom = ((contraTC - ipsiTC) ./ (contraTC + ipsiTC));
data.auralDomVals = data.auralDom(data.unionIO == 1);
data.auralDomInd = mean(data.auralDomVals(:));
data.auralDom(isnan(data.auralDom))=0;

if isnan(data.auralDomInd)
    error('Null result! Pay attention that you did not use the same map twice!')
end

% Intersection Aural Dominance
% Second definition where the combinations contained in the intersect are
% used
data.intersectIO = zeros(size(data.auralDom));
% Note that the only difference is using & instead of |!
data.intersectIO(contraIoGrid==1 &...
    ipsiIoGrid==1) = 1;

data.interAuralDomVals = data.auralDom(data.intersectIO == 1);
data.interAuralDomInd = mean(data.interAuralDomVals(:));


% Receptive Field Overlap
% Defined as percent of points in intersect divided by points in union
data.RFO = sum(data.intersectIO)/sum(data.unionIO);

% Freq differences
% Since we measure in octaves, log2 must be used! arithmetic differences
% would have no meaning!
data.BfDiff = log2(contraDat.BF/ipsiDat.BF);
data.absBfDiff = abs(data.BfDiff);

data.CfDiff = log2(contraDat.CF/ipsiDat.CF);
data.absCfDiff = abs(data.CfDiff);

%Threshold Differences
data.thrDiff = contraDat.thr - ipsiDat.thr;

%% Figures
% 
% uIoFig = figure; 
% surf(contraStimFreqs,contraStimLevels,data.unionIO); view(2); title('Union IO')
% set(gca, 'XScale','log')
% 
% contraFig = figure; 
% surf(contraStimFreqs,contraStimLevels,contraIoGrid);view(2); title('Contra')
% set(gca, 'XScale','log')
% ipsiFig = figure; 
% surf(ipsiStimFreqs,ipsiStimLevels,ipsiIoGrid);view(2); title('Ipsi')
% set(gca, 'XScale','log')
% 
% uAdFig = figure; 
% surf(contraStimFreqs,contraStimLevels,data.auralDom); colorbar; view(2)
% set(gca, 'XScale','log')
% title('Union Aural Dominance')
% caxis([-1 1])
% colormap('hot')
% 
% iAdFig = figure; 
% surf(contraStimFreqs,contraStimLevels,data.auralDom.*data.intersectIO); colorbar; view(2)
% title('Intersect AD')
% set(gca, 'XScale','log')
% caxis([-1 1])
% colormap('hot')
% 
% intIoFig = figure; 
% surf(contraStimFreqs,contraStimLevels,data.intersectIO); view(2)
% title('Intersect IO')
% set(gca, 'XScale','log')
% 
% close all

