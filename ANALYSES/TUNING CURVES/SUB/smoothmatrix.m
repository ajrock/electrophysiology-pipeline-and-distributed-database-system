function smoothed_data = smoothmatrix(data, filt)

% this function smooths a matrix with the filter (filt). At the borders and
% corners the corresponsing subset of filt is used and NO addition zeros
% surrounding original matrix data are used. 
% The example filter is designed, that the original value (middle point)
% has 50% of weight. If only a subet is used this relative weight changes
% to 8/11 in the corners and 8/13 at the borders. 

% handle wrong filters
if isempty(filt) % if filter is not defined use standard. 
  filt = [1 1 1; 1 2 1; 1 1 1]; % original value in the middle weights 50%
  %filt = filt./sum(sum(filt));  % sum of weights is 1. 
                                % No change of overall spikerate
end

if size(filt,1) ~= size(filt,2)
  warning('Filter must have same number of lines and columns! Standard Filter is used instead.')
  filt = [1 1 1; 1 2 1; 1 1 1]; % original value in the middle weights 50%
  %filt = filt./sum(sum(filt));  % sum of weights is 1. 
                                % No change of overall spikerate
end

if ~rem(size(filt,1),2) 
  warning('Filter must have odd number of lines and columns! Standard Filter is used instead.')
  filt = [1 1 1; 1 2 1; 1 1 1]; % original value in the middle weights 50%
  %filt = filt./sum(sum(filt));  % sum of weights is 1. 
                                % No change of overall spikerate
end

% initialize
smoothed_data = nan(size(data));
midind = (size(filt,1)+1)/2; % index of middle of filter
fiwi = midind-1; %width of filter

%% smoothing for different cases
% 1.case: four corners
smoothed_data(1,1) = sum(sum( ...
    data(1:midind, 1:midind).* filt(midind:end, midind:end) )) ...
    ./ sum(sum(filt(midind:end, midind:end)));
smoothed_data(1,end) = sum(sum( ...
    data(1:midind, end-fiwi:end).* filt(midind:end, 1:midind) )) ...
    ./ sum(sum(filt(midind:end, 1:midind)));
smoothed_data(end,1) = sum(sum( ...
    data(end-fiwi:end, 1:midind).* filt(1:midind, midind:end) )) ...
    ./ sum(sum(filt(1:midind, midind:end)));
smoothed_data(end,end) = sum(sum( ...
    data(end-fiwi:end, end-fiwi:end).*filt(1:midind, 1:midind)))...
    ./ sum(sum(filt(1:midind, 1:midind)));

% 2. case: borders
bofilt = filt(midind:end, :); % top border
bofiltsum = sum(sum(bofilt));
for i=2:(size(data,2)-1)
  smoothed_data(1,i) = ...
    sum(sum( data(1:midind,i-fiwi:i+fiwi).* bofilt)) ./ bofiltsum;
end

bofilt = filt(:, 1:midind); % right border
bofiltsum = sum(sum(bofilt));
for i=2:(size(data,1)-1)
  smoothed_data(i,end) = ...
    sum(sum( data(i-fiwi:i+fiwi, end-fiwi:end).* bofilt)) ./ bofiltsum;
end

bofilt = filt(1:midind, :); % bottom border
bofiltsum = sum(sum(bofilt));
for i=2:(size(data,2)-1)
  smoothed_data(end,i) = ...
    sum(sum( data(end-fiwi: end,i-fiwi:i+fiwi).* bofilt)) ./ bofiltsum;
end

bofilt = filt(:, midind:end); % left border
bofiltsum = sum(sum(bofilt));
for i=2:(size(data,1)-1)
  smoothed_data(i,1) = ...
    sum(sum( data(i-fiwi:i+fiwi, 1:midind).* bofilt)) ./ bofiltsum;
end

% 3.case: inside area
smoothed_data(2:end-1, 2:end-1) = ...
    filter2(filt./sum(sum(filt)),data, 'valid');

% programmed by Armin Wiegner, June 2016
