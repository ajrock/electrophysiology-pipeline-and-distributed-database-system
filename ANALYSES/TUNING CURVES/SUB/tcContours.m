function [axHandle] = tcContours(axHandle, dB, freqs, data)
%TCCONTOURS Takes incoming axes handle and plots the relevant X-Y
%information on a contour plot of the tuning curve desired.

[X, Y] =  meshgrid(freqs,dB);
if length(unique(data))==2
    contourf(axHandle,X,Y,data,[0 1]);
else
    contourf(axHandle,X,Y,data,5);
end
set(axHandle,'xscale','log');
axHandle.XTick=[500 1000 2000 4000 8000 16000];
axHandle.XTickLabel={'0.5','1','2','4','8','16'};
axHandle.XLabel.String='Freq [kHz]';
axHandle.YLabel.String='Stim Level [dB]';

end

