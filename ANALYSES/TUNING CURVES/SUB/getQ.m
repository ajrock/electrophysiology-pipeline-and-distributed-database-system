function outDat = getQ(inDat)
% outDat is data structure to place results, ii is iterative position, for 
% storing results in outDat, inDat is remote data structure to be read

% If no Cf, this analysis does not make sense!
if isnan(inDat.CF)|| isempty(inDat.CF)
    outDat = NaN;
    return
end

% Abs 70/80 results
toDo = [45 70 75 80];
for ind = 1:length(toDo)
    fieldNames = strcat({'cfQabs','lowerFreqAbs','upperFreqAbs','f0QAbs','diffCfF0Abs'},num2str(toDo(ind)));
    [~, minInd]=min(abs(inDat.levels-toDo(ind)));
    if abs(inDat.levels(minInd)-toDo(ind))<5
        srchRow = inDat.io(minInd,:);
    else
        srchRow = [];
    end
    if isempty(srchRow) || inDat.thr > toDo(ind)
        for jj = 1:length(fieldNames)
            outDat.(fieldNames{jj}) = NaN;
        end
    else
        [outDat.(fieldNames{1}) outDat.(fieldNames{2}) outDat.(fieldNames{3})...
            outDat.(fieldNames{4}) outDat.(fieldNames{5})] = ...
            bwChar(inDat.CF,inDat.freqs,srchRow);
    end
end

% Rel results
%cfLvl = find(inDat.thr==inDat.levels);
maxLvl = max(inDat.levels)-inDat.thr;
if maxLvl~=0
toDo = [10 15 20 30];
for ind = 1:length(toDo)
    fieldNames = strcat({'cfQrel','lowerFreqRel','upperFreqRel','f0Qrel','diffCfF0rel'},num2str(toDo(ind)));
    [~, minInd]=min(abs(inDat.levels-toDo(ind)));
    if abs(inDat.levels(minInd)-toDo(ind))<5
        srchRow = inDat.io(minInd,:);
    else
        srchRow = [];
    end
    srchRow = inDat.io(inDat.levels-inDat.thr==toDo(ind),:);
    if toDo(ind)>maxLvl || isempty(srchRow)
        for jj = 1:length(fieldNames)
            outDat.(fieldNames{jj}) = NaN;
        end
    else
        [outDat.(fieldNames{1}) outDat.(fieldNames{2}) outDat.(fieldNames{3})...
            outDat.(fieldNames{4}) outDat.(fieldNames{5})] = ...
            bwChar(inDat.CF,inDat.freqs,srchRow);
    end
end

end
end

function [cfQ,lowerF,upperF,f0Q,diffCfF0] = bwChar(cf,freqs,srchRow)

% Following algorithm uses the last zero value to determine true lower
% bound, otherwise other smaller regions of activity could make an
% artificially low bound
lowerInd = find(~srchRow(freqs<=cf),1,'last')+1;
if lowerInd == length(freqs)+1
    lowerInd = length(freqs);
end
lowerF = freqs(lowerInd);
% This removes possibility of detecting lower bounds while searching
% for upper bound
srchRow(1:lowerInd+1)=1;
upperInd = find(~srchRow,1)-1;
if upperInd == 0
    upperInd = 1;
end
upperF = freqs(upperInd);
% Error checking, got to boundary condition before finding a valid value
if isempty(lowerF) || isempty(upperF)
    [cfQ,lowerF,upperF,f0Q,diffCfF0] = deal(NaN);
    return
end
BW = upperF-lowerF;
cfQ = cf/BW;
f0 = geomean([lowerF upperF]);
f0Q = f0/BW;
diffCfF0 = log2(cf)-log2(f0);

end