function [in_out_grid, tc_data, IBT] = BlobFinder(tc_data, varargin)

% 2017-04-10, AC: Added triple pass mean filter for smoothing smear correction
% 2017-10-20, AC: Changed Auto IBTM to min for elMask situation, otherwise
% threshold is set far too high
% 2018-07-31, AC: changed initial column from max of mean columns to simply
% max of highest stim level, to avoid damage from noisy data

IBTMS = varargin{1};
above_trig = varargin{2};

% tc_data is the tuning curve (TC). Matrix of spikerates with columns are
% stimfreq (increasing) and rows stimlevel (increasing from top to down!). 
% above_trig = 1 -> find tuning for activity above trigger
% above trig = 0 -> find tuning for activity below trigger
if ~sum(above_trig == [0 1])
    disp('above_trig is not set correctly!')
    above_trig = 1;
end

%Calculate spontaneous activity (cut off for belonging in a tc will be some
%threshold above spontaneous) - based on responses to 0 db tones

%% calculate parameters
if above_trig
  %Identify the column with the highest overall firing rate.
  [high_rat,high_col] = max(mean(tc_data,1));
  %Within that column, identify the point of highest firing
  [high_pt, high_row] = max(tc_data(:,high_col));
else
  %Identify the column with the lowest overall firing rate.
  [high_rat,high_col] = min(mean(tc_data,1));
  %Within that column, identify the point of highest firing
  [high_pt, high_row] = min(tc_data(:,high_col));
end

%AW: IF we do not have 0 dB level, than use min level instead but for all
%frequencies instead of the 5 frequencies surrounding the CF (2+CF+2). 
spon_act = max([mean(tc_data(1,[1:high_col-2 high_col+2:end])) 0.05]); 
% If the spontaneous activity is too low, use 0.05 instead
% inner_blob_thresh = max([spon_act*5]);
max_act = max(max(tc_data));
%p = 90; % unused!
p = 80; % for percentile in the following

driven_act = tc_data(find(tc_data>spon_act)); %just vector, matrix strucure is lost
top20_act = prctile(driven_act, p); % p'th percentile of driven activity
bottom20_act = prctile(driven_act, 20);
% inner_blob_thresh = spon_act + (0.33333 * (max_act-spon_act));
if above_trig
    inner_blob_thresh1 = spon_act + (0.3* (top20_act-spon_act)); %previously 0.3333333 * pot10 act previously tried 20% as well... 
else
    inner_blob_thresh1 = spon_act - (0.3* (bottom20_act-spon_act));
end
    % 0.2* (top20_act-spon_act), before (AW)

% Alternative SA calculation (AW)
if high_col > 5 && high_col <= size(tc_data,2)-5
  NR_data = [tc_data(1,[1:high_col-2 high_col+2:end]) ...
      tc_data(2,[1:high_col-3 high_col+3:end]) ...
      tc_data(3,[1:high_col-5 high_col+5:end])];
elseif high_col <= 5
  NR_data = [tc_data(1,high_col+2:end) ...
      tc_data(2,high_col+3:end) ...
      tc_data(3,high_col+5:end)];
elseif high_col > size(tc_data,2)-5
  NR_data = [tc_data(1,1:high_col-2) ...
      tc_data(2,1:high_col-3) ...
      tc_data(3,1:high_col-5)];
else
  warning('   Something is poorly defined!')
end
spon_mean = mean(NR_data(:));
spon_std = std(NR_data(:));
spon_act2 = max([spon_mean 0.05]); %If the spontaneous activity is too low, use 0.05 instead
if above_trig
    inner_blob_thresh2 = spon_act2 + 3*spon_std; % 2* before
else
    inner_blob_thresh2 = spon_act2 - 1.5*spon_std; % 2* before actually was 3
end
% Adding check code for if calculated spont rate is accidentally higher
% than the max response rate!
if inner_blob_thresh1 > max(tc_data,[],'all')
    inner_blob_thresh1 = 0.05;
end
if inner_blob_thresh2 > max(tc_data,[],'all')
    inner_blob_thresh2 = 0.05;
end
% Use the maximum of both alternatives as threshold(SA):
switch IBTMS
  case 0
      if above_trig
           [inner_blob_thresh inner_blob_thresh_method] = ...
              max([inner_blob_thresh1 inner_blob_thresh2]);
      else
          [inner_blob_thresh inner_blob_thresh_method] = ...
              min([abs(inner_blob_thresh1) abs(inner_blob_thresh2)]);
      end
  case 1
    inner_blob_thresh = inner_blob_thresh1;
    inner_blob_thresh_method = 1;
  case 2
    inner_blob_thresh = inner_blob_thresh2;
    inner_blob_thresh_method = 2;
  otherwise
    disp('   Something is bad defined with IBTM in GUI!')
    [inner_blob_thresh inner_blob_thresh_method] = ...
        max([inner_blob_thresh1 inner_blob_thresh2]);
end
[inner_blob_thresh inner_blob_thresh_method];
IBT.inner_blob_thresh1 = inner_blob_thresh1;
IBT.inner_blob_thresh2 = inner_blob_thresh2;
IBT.inner_blob_thresh = inner_blob_thresh;
IBT.inner_blob_thresh_method = inner_blob_thresh_method;
IBT.spon_act_Kil = spon_act;
IBT.spon_act_AW = spon_mean; 
IBT.spon_SD_AW = spon_std;

% FIR filter changed to do multiple, weak filtering rather than one strong
% one
FIR_filter = [1 1 1;1 10 1;1 1 1]; %added by AC
tc_data = smoothmatrix(tc_data, FIR_filter);
tc_data = smoothmatrix(tc_data, FIR_filter); %added by AC
tc_data = smoothmatrix(tc_data, FIR_filter); %added by AC

if above_trig
  %Identify the column with the highest overall firing rate.
  [high_rat,high_col] = max(tc_data(end,:));
  %Within that column, identify the point of highest firing
  [high_pt, high_row] = max(tc_data(:,high_col));
else
  %Identify the column with the lowest overall firing rate.
  [high_rat,high_col] = min(mean(tc_data,1));
  %Within that column, identify the point of highest firing
  [high_pt, high_row] = min(tc_data(:,high_col));
end

%% Add a row of strongly evoked activity to the 'top' of the tuning curve
%(really the bottom of the matrix) this will allow us to add all the
%'peaks' along the top of the tuning curve
% AW: Multiple tunings are connected via this artificial suprathreshold
% responses at levels > max stimlevel. 
%tc_data_top_row = add_top_row(tc_data(end,:), inner_blob_thresh); % Kilgard
if above_trig
    tc_data_top_row = ones(1, size(tc_data, 2))*1.5*inner_blob_thresh; % simple, AW
    % Yeah, was simple until you forget that sometimes you wanna check
    % UNDER the threshold...
else
    tc_data_top_row = zeros(1, size(tc_data, 2));
end
tc_data = [tc_data; tc_data_top_row]; %AW: faster

%% find in_out_grid; digitize TC
%Two grids - one to keep track of 'in' or 'out' of tc, the other to keep
%track of whether or not we've actually looked at the data
in_out_grid = zeros(size(tc_data));
checked_grid = zeros(size(tc_data));

% initial value for searching of tuning
check_pts_xy = [high_row, high_col]; %row/column indices for data points in tc
check_pts_ind = sub2ind(size(tc_data),check_pts_xy(:,1), check_pts_xy(:,2));
% while sum(sum(checked_grid~=0)) 
%continue until you've checked all parts of the grid
while ~isempty(check_pts_ind)
% for i = 1:20
    %1.  Check whether or not tc_pts below in tc
    if above_trig
      in_tc_pts = find(tc_data(check_pts_ind)>=inner_blob_thresh);
    else
      in_tc_pts = find(tc_data(check_pts_ind)<inner_blob_thresh);
    end
    
    %2.  Update in/out grid and check grid
    checked_grid(check_pts_ind) = 1;
    in_out_grid(check_pts_ind(in_tc_pts)) = 1;
    
    %3.  Find all the neighbors of checked points which are IN tuning curve
    nw_shift = [-1, -1];
    n_shift = [-1,0];
    ne_shift = [-1, 1];
    e_shift = [0, 1];
    se_shift = [1,1];
    s_shift = [1,0];
    sw_shift = [1, -1];
    w_shift = [0,-1];
    
    % concatunate new indices to check
    nw_set = check_pts_xy(in_tc_pts,:) + repmat(nw_shift,size(check_pts_xy(in_tc_pts,:),1),1);
    n_set = check_pts_xy(in_tc_pts,:) + repmat(n_shift,size(check_pts_xy(in_tc_pts,:),1),1);
    ne_set = check_pts_xy(in_tc_pts,:) + repmat(ne_shift,size(check_pts_xy(in_tc_pts,:),1),1);
    e_set = check_pts_xy(in_tc_pts,:) + repmat(e_shift,size(check_pts_xy(in_tc_pts,:),1),1);
    se_set = check_pts_xy(in_tc_pts,:) + repmat(se_shift,size(check_pts_xy(in_tc_pts,:),1),1);
    s_set = check_pts_xy(in_tc_pts,:) + repmat(s_shift,size(check_pts_xy(in_tc_pts,:),1),1);
    sw_set = check_pts_xy(in_tc_pts,:) + repmat(sw_shift,size(check_pts_xy(in_tc_pts,:),1),1);
    w_set = check_pts_xy(in_tc_pts,:) + repmat(w_shift,size(check_pts_xy(in_tc_pts,:),1),1);

    check_set = [nw_set; n_set; ne_set; e_set; se_set; s_set; sw_set; w_set];
    
    %4a.  Eliminate overlaps from nw, n, ne set, etc.
    check_set = unique(check_set, 'rows');
    
    %4b.  Eliminate any neighbors which don't actually exist (too high or
    % too low x/y)
    bad_row = find((check_set(:,1)<1) | (check_set(:,1) > size(tc_data,1)));
    bad_col = find(check_set(:,2)<1 | check_set(:,2) > size(tc_data,2));
    
    if ~isempty([bad_row; bad_col])
        % setdiff will change in future Matlab releases!!!
        check_set = check_set(setdiff(1:size(check_set,1),[bad_row; bad_col]),:);
    end
    %4c.  Eliminate any neighbors which have already been checked
    
    check_set_ind = sub2ind(size(tc_data), check_set(:,1), check_set(:,2));
    
    check_pts_ind = check_set_ind(find(checked_grid(check_set_ind)==0));
    [r_ck, c_ck] = ind2sub(size(tc_data), check_pts_ind);
    check_pts_xy = [r_ck c_ck];
    
end

% Delete artificial added top row
tc_data = tc_data(1:end-1, :);
in_out_grid = in_out_grid(1:end-1,:);
in_out_grid = fixIo(in_out_grid);
checked_grid = checked_grid(1:end-1,:);

end


function io = fixIo(io)
% Small algorithm to fix holes in IO grid causing weird effects for finding
% parameters.

temp = io;
temp(end+1,:) = deal(1);
ccFI=bwconncomp(temp,4);
result = regionprops(ccFI,'FilledImage');
[~,imageInd] = max(arrayfun(@(x) numel(x.FilledImage),result));
filledImage = result(imageInd).FilledImage;
io(1+end-size(filledImage(1:end-1,:),1):end,:) = filledImage(1:end-1,:);

end
