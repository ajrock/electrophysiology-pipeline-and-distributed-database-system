function status = ioAnalysis(data)
%ioAnalysis Summary of this function goes here
%   Detailed explanation goes here

%% Fix stimulus parameters

data.stimType = determineStimType();
check = 0;
while check == 0
data.stimShape = input('1: (p)ulse, 2: (c)lick, or 3: (n)oise: ','s');
switch data.stimShape
    case {'p','1'}
        data.stimShape = 'Pulse';
        check = 1;
    case {'c','2'}
        data.stimShape = 'Click';
        check = 1;
    case {'n','3'}
        data.stimShape = 'Noise';
        check = 1;
    otherwise
        fprintf('Pick a valid option!')
end
end

data.hemisphere = determineHemisphere(data.fileName);

offStim = 120;

left.stim = data.stimGrid(data.keyInds(1),:);
right.stim = data.stimGrid(data.keyInds(2),:);

if strcmp(data.stimType,'a')
    % max value is compared to lowest stimulation level.
    % Need to consider minimum dB SPL at NOT 120 att, as the levels are
    % not the same.
    dbSplCol = find(endsWith(data.paramNames, {'dB SPL','dB_SPL'}));
    dbSpl = data.stimGrid(dbSplCol,:);
    if isempty(dbSplCol)
        % Poorly conditioned data from MV
        maxDB = [];
        while isempty(maxDB)
            fprintf(['This file does not track dB SPL. \nPlease enter ',...
                'the dB SPL output when attenuation = 0'])
            maxDB = str2double(input(': ','s'));
        end
        left.stim = maxDB - left.stim;
        right.stim = maxDB - right.stim;
        offStim = 0;
    elseif size(dbSpl,1) == 2 % More than one column, old data
        dbSplColL = find(endsWith(data.paramNames, 'S1 dB SPL'));
        dbSplColR = find(endsWith(data.paramNames, 'S2 dB SPL'));
        dbSplL = data.stimGrid(dbSplColL,:);
        dbSplR = data.stimGrid(dbSplColR,:);
        left.stim(left.stim<offStim) = 100-dbSplL(left.stim<offStim);
        right.stim(right.stim<offStim) = 100-dbSplR(right.stim<offStim);
        % These types had 0 dB labeled as 100 instead of 120...
        left.stim(left.stim == 100) = offStim;
        right.stim(right.stim == 100) = offStim;
    else
        %left.stim(left.stim<offStim) = 100-dbSpl(left.stim<offStim);
        %right.stim(right.stim<offStim) = 100-dbSpl(right.stim<offStim);
        left.stim(left.stim<offStim) = dbSpl(left.stim<offStim);
        right.stim(right.stim<offStim) = dbSpl(right.stim<offStim);
    end
else
    left.stim(left.stim ~=offStim) = 0 - left.stim(left.stim ~=offStim);
    right.stim(right.stim ~=offStim) = 0 - right.stim(right.stim ~=offStim);
end

% Check for offset in amplitude for electric IOs
if isempty(regexpi(data.fileName, 'offset')) % no offset
    offset = 0;
else
    offsetLoc = regexpi(data.fileName, 'offset');
    usLocs = regexpi(data.fileName, '_');
    offsetStart = usLocs(find(usLocs<offsetLoc,1,'last'));
    offsetEnd = usLocs(find(usLocs>offsetLoc,1));
    whereIsOffset = regexp(data.fileName(offsetStart:offsetEnd),'\d');
    offset = str2double(data.fileName(offsetStart+whereIsOffset-1));
    if isnan(offset)
        offset = left.stim(1) - right.stim(1); % hope that its spot #1?
    end
end

%% Detect sided data
[ipsi, contra, bds] = detectSidedData(data,left,right,offset,offStim);

% Correction for bad data
ipsi.levels = unique(ipsi.stim(ipsi.stimIndices));
contra.levels = unique(contra.stim(contra.stimIndices));
bds.levels = unique(left.stim(bds.stimIndices));

if strcmp(data.stimType,'e')
    ipsi.levels = fliplr(ipsi.levels);
    contra.levels = fliplr(contra.levels);
    bds.levels = fliplr(bds.levels);
end

%ipsi.data = ipsi.data(:,:,ipsi.stimIndices(1:length(ipsi.levels)));
%ipsi.stimGrid = ipsi.stimGrid(:,ipsi.stimIndices(1:length(ipsi.levels)));

%% Data Calculations
ipsi.repCount   =  squeeze(sum(cellfun(@numel,ipsi.data ),1));
contra.repCount  =  squeeze(sum(cellfun(@numel,contra.data),1));
bds.repCount    =  squeeze(sum(cellfun(@numel,bds.data  ),1));

ipsi.meanCount  = mean(ipsi.repCount, 1);
contra.meanCount = mean(contra.repCount,1);
bds.meanCount   = mean(bds.repCount,  1);

ipsi.stdCount  = std(ipsi.repCount, 1);
contra.stdCount = std(contra.repCount,1);
bds.stdCount   = std(bds.repCount,  1);

%% Check output and decide whether to proceed
checkFig = figure('Position', [10 50 560 420],'Name','RawRespAndFit');
bdsPlot = subplot(131);
if ~isempty(bds.stimIndices)
errorbar(bds.levels,bds.meanCount,bds.stdCount);
title('binaural')
end
xlabel('Stim Level [dB]')
ylabel('Evoked Response [spks/rep over window]')
ipsiPlot = subplot(132);
if ~isempty(ipsi.stimIndices)
errorbar(ipsi.levels,ipsi.meanCount(1:length(ipsi.levels)),ipsi.stdCount(1:length(ipsi.levels)));
title('ipsi')
end
contraPlot = subplot(133);
if ~isempty(contra.stimIndices)
errorbar(contra.levels,contra.meanCount,contra.stdCount);
title('contra')
end

cont = input('Continue? Y/N: ','s');
r = regexpi(cont,{'n','0'});

if any([r{:}])
    status = 0;
    data.ipsi = ipsi;
    data.contra = contra;
    data.bds = bds;
else
    checkFig;
    if strcmp(data.stimType,'e')
        x = min([ipsi.levels,contra.levels,bds.levels])-5:0.25:max([ipsi.levels,contra.levels,bds.levels])+5;
    else
        x = 0:1:100;
    end
    if ~isempty(bds.stimIndices)
        bds.vals = sigFit(bds.levels,bds.meanCount,data.stimType);
        subplot(131)
        hold on
        plot(x,bds.vals(x))
        hold off
    end
    if ~isempty(ipsi.stimIndices)
        ipsi.vals = sigFit(ipsi.levels,ipsi.meanCount(1:length(ipsi.levels)),data.stimType);
        subplot(132)
        hold on
        plot(x,ipsi.vals(x))
        hold off
    end
    if ~isempty(contra.stimIndices)
        contra.vals = sigFit(contra.levels,contra.meanCount,data.stimType);
        subplot(133)
        hold on
        plot(x,contra.vals(x))
        hold off
    end
    
    cont = input('Continue? Y/N: ','s');
    % Have to look to ensure the fit is good. If at this point not, exit
    % the program and debug
    if any(regexpi(cont,'n'))
        error('Fit went bad!')
    end
    
    %% Response parameters
    invertedSig = @(y, vals) vals(3)/4 * erfinv((y-vals(4))/vals(1)) + vals(2);
    legFlag = 1;
    latFig = figure('Position', [10 50 560 420],'Name','Latencies'); movegui(latFig,'northeast');
    if ~isempty(bds.stimIndices)
        bds = respParams(bds,bdsPlot,strcmp(data.stimType,'a'));
        s=subplot(131);
        errorbar(bds.levels,bds.firstLat,bds.firstLatStd);
        tempLim = sort([bds.levels(end) bds.levels(1)]);
        s.XLim = [tempLim(1)-1 tempLim(2)+1];
        s.YLim=[0 s.YLim(2)+2];
        xlabel('Stim Level [dB]')
        ylabel('Uncorr Latency re: sweep onset [ms]')
        legend(bdsPlot,{'Raw Mean/error','SigFit','thr','sat'})
        legFlag = 0;
    end
    if ~isempty(ipsi.stimIndices)
        ipsi = respParams(ipsi,ipsiPlot,strcmp(data.stimType,'a'));
        s=subplot(132);
        errorbar(ipsi.levels,ipsi.firstLat,ipsi.firstLatStd);
        tempLim = sort([ipsi.levels(end) ipsi.levels(1)]);
        s.XLim = [tempLim(1)-1 tempLim(2)+1];
        s.YLim=[0 s.YLim(2)+2];
        if legFlag
            xlabel('Stim Level [dB]')
            ylabel('Uncorr Latency re: sweep onset [ms]')
            legend(ipsiPlot,{'Raw Mean/error','SigFit','thr','sat'})
            legFlag = 0;
        end
    end
    if ~isempty(contra.stimIndices)
        contra = respParams(contra,contraPlot,strcmp(data.stimType,'a'));
        s=subplot(133);
        errorbar(contra.levels,contra.firstLat,contra.firstLatStd);
        tempLim = sort([contra.levels(end) contra.levels(1)]);
        s.XLim = [tempLim(1)-1 tempLim(2)+1];
        s.YLim=[0 s.YLim(2)+2];
        if legFlag
            xlabel('Stim Level [dB]')
            ylabel('Uncorr Latency re: sweep onset [ms]')
            legend(contraPlot,{'Raw Mean/error','SigFit','thr','sat'})
        end
    end
    
    if ~isempty(ipsi.stimIndices) & ~isempty(contra.stimIndices)
        dom = dominance(contra,ipsi);
    else
        dom = [];
    end
    
    %% Save and mark status
    cont = input('Continue? Y/N: ','s');
    if any(regexpi(cont,'n'))
        status = 0;
    else
        status = 1;
    end
    
    data.bds = bds;
    data.contra = contra;
    data.ipsi = ipsi;
    data.dom = dom;
    data.status = status;
    
    save([data.folder '\' data.fileName],'data')
    writeData(data)
    
end

end

function writeData(data)
% Barf the data into an excel because this is easier than teaching people
% to code. 

% using function that unpacks nested structures into a cellarray of tables.
% Will in/exclude data from here
% Keep: 1, 3,4,5,7,8,9,12,13,15,16,17,19,20,21,24
% Modify: 2,14,25
% delete: 6,10,11,18,22,23
rmThese = {'folder','keyInds','fileName','status'};
cellTabs = unpackStruct2table(data);
for ii = 1:length(rmThese)
    cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}.(rmThese{ii})=[];
end
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}.sourceFiles = data.srcFile;
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'contra')}.contra_tcKeyInds= [];
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'ipsi')}.ipsi_tcKeyInds= [];
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')} = ...
    [cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}...
    cell2table({input('Give a Comment: ','s')},...
    'VariableNames',{'Comment'})];

ii = 1;
while ii <= length(cellTabs)
    if numel(cellTabs{ii}) == 1
        cellTabs(ii) = [];
    else
        cellTabs{ii}.('Filename') = data.fileName;
        cellTabs{ii} = movevars(cellTabs{ii},'Filename','Before','TableTitle');
        ii = ii+1;
    end
end


newFold = fullfile(data.folder,data.fileName);
if ~exist(newFold,'dir')
    mkdir(newFold)
end

for ii = 1:numel(cellTabs)
    if exist([fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'],'file')
        delete([fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'])
    end
    writetable(cellTabs{ii},[fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'])
end


end

function data = dominance(contra,ipsi)

%% Fit Values

if any(contra.levels>0)
    minX = round(min([0,contra.threshold,ipsi.threshold]),2);
    x = minX:0.01:100;
else
    x = min([contra.levels, ipsi.levels]):0.01:max([contra.levels, ipsi.levels]);
end

C = contra.vals(x);
C(x<floor(contra.threshold))=0;
I = ipsi.vals(x);
I(x<floor(ipsi.threshold))=0;
absThr = min(contra.threshold,ipsi.threshold);
absSat = max(contra.saturation,ipsi.saturation);

data.dom = (C-I) ./ (C+I);
data.dom(isnan(data.dom))=0;
data.domVal = mean(data.dom(x>absThr&x<absSat));
data.domMed = median(data.dom(x>absThr&x<absSat));

figure('Position', [10 50 560 420],'Name','AuralDominance'); plot(x,data.dom)
hold on
plot([absThr absSat],[data.domVal data.domVal])
plot([absThr absSat],[data.domMed data.domMed])

if any(contra.levels>0)
    data.domVal70 = data.dom(x==70);
    data.domVal80 = data.dom(x==80);
    scatter(70,data.domVal70)
    scatter(80,data.domVal80)
end
midRange = round((absSat+absThr)/2,2);
data.domValMidRange = data.dom(find(x-midRange<0.01&x-midRange>-0.01,1));

if ~isempty(data.domValMidRange)
    scatter(round((absSat+absThr)/2,2),data.domValMidRange)
end

hold off
legend({'Dom','Union','Median','70 dB','80 dB','Mid Range'})
movegui('northwest');
xlabel('Stim Level [dB]')
ylabel('Dominance (C-I)/(C+I)')

end


function data = respParams(data,currentPlot,isAcou)
% invertedSig = @(y, vals) vals(3)/4 * erfinv((y-vals(4))/vals(1)) + vals(2);
invertedSig = @(y,vals) vals(2)+vals(3)/4*erfinv((y-vals(4))/vals(1)-1);

vals = coeffvalues(data.vals);
% set too small values to zero
if vals(1) < 0.1 %|| vals(4) < 0.1 % old vals(4) removed, reflects a different parameter now
    if any(data.levels>0) % acoustic
        data.saturationSpikeRate = 0;
        
        data.threshold = 100;
        data.saturation = 0;
        data.dynamicRange = 0;
    else
        data.saturationSpikeRate = 0;
        
        data.threshold = 0;
        data.saturation = -100;
        data.dynamicRange = 0;
    end
else
    data.saturationSpikeRate = vals(1)*2; %+vals(4); % see above
    data.threshold = invertedSig(0.05*vals(1)*2,vals); % see above
    data.saturation = invertedSig(vals(1)*0.95*2,vals); % see above
    data.dynamicRange = data.saturation - data.threshold;
end

hold(currentPlot);
scatter(currentPlot,data.threshold,0.05*vals(1)*2,'*')
scatter(currentPlot,data.saturation,vals(1)*0.95*2,'o')
hold off

%% Latency
fixEmpty = cellfun(@isempty,data.data);
latMap = data.data; latMap(fixEmpty) = {NaN};
latMap = cellfun(@(x) x(1), latMap);
data.firstLat = squeeze(mean(latMap,[1 2],'omitnan'));
data.firstLatStd = squeeze(std(latMap,0,[1 2],'omitnan'));
data.thresholdLatency = interp1(data.levels,data.firstLat,data.threshold);
data.saturationLatency = interp1(data.levels,data.firstLat,data.saturation);
data.thresholdLatencyStd = interp1(data.levels,data.firstLatStd,data.threshold);
data.saturationLatencyStd = interp1(data.levels,data.firstLatStd,data.saturation);
if isAcou
    data.relThr20Latency = interp1(data.levels,data.firstLat,data.threshold+20);
    data.relThr20LatencyStd = interp1(data.levels,data.firstLatStd,data.threshold+20);
else
    data.relThr2Latency = interp1(data.levels,data.firstLat,data.threshold+2);
    data.relThr2LatencyStd = interp1(data.levels,data.firstLatStd,data.threshold+2);
end

end



function vals = sigFit(x, spikes,stimType)
% a = amplitude, b = horizontol midline (50% response strength), c =
% dynamic range, d = spont act
% Correct vectors to be 1 x N
if size(x,1) == 1
    x = x';
end
if size(spikes,1) == 1
    spikes = spikes';
end

if strcmp(stimType,'a')
    startPoint = [1     50      50  0];
    lowerBound = [0   -50      5   0];
    upperBound = [50    100     100 20];
    x2 = (0:5:100)';
    y = zeros(length(x2),1);
    weights = ones(length(x2),1)*.5;
    [~,b]=intersect(x2,round(x/5)*5);
    y(b)=spikes;
    weights(b) = 1;
    % Set weight to zero for missing steps within original range (if
    % different step sizes used)
    weights(setdiff(b(1):b(end),b)) = 0;
    y(x2>max(x)) = max(spikes);
    x = x2;
else
    startPoint = [1     mean([x(1),x(end)])   abs(diff([x(1),x(end)]))  0];
    lowerBound = [0.1   x(end)                    0.1                   0];
    upperBound = [20    x(1)              abs(diff([x(1),x(end)]))*2   20];
    stepSize = x(2)-x(1);
    x2 = (x(1)-stepSize*5:stepSize:x(end)+stepSize*5)';
    y = zeros(length(x2),1);
    weights = ones(length(x2),1)*.5;
    [~,b]=intersect(round(x2,1),round(x,1)); b = flipud(b);
    y(b)=spikes;
    weights(b) = 1;
    weights(setdiff(b(1):b(end),b)) = 0;
    y(x2>max(x)) = max(spikes);
    x = x2;
end

%fitFunc = @(a,b,c,d,x) a*erf((x-b)/(c/4))+d; % Old fitting function
% New form, with amplitude linked to offset so that sigmoid values are
% 'always' positive: d now strictly reflects spontaneous/low level response
% rates
fitFunc = @(a,b,c,d,x) a*(1+erf((x-b)/(c/4)))+d; 
vals = fit(x,y,fitFunc,'Lower',lowerBound,'Upper',upperBound,'StartPoint',startPoint,'Weights',weights);

% Next section tests for fit going lower than 0. If so, restrict range so
% this does not happen
% if (vals.d-vals.a)<0
%     a = vals.a; d = vals.d;
%     vals.a = (a+d)/2; vals.d = vals.a;
% end
end






