function status = rrtfAnalysis(data)
%ioAnalysis Summary of this function goes here
%   Detailed explanation goes here

%% Fix stimulus parameters
data.fileName
data.stimType = determineStimType();

switch data.stimType
    case {'Electric','e'}
        data.stimShape = 'Pulse';
    case {'Acoustic','a'}
        data.stimShape = 'Click';
end

data.hemisphere = determineHemisphere(data.fileName);
data = determineWindowMethod(data);

offStim = 0;
offset = 0;

% Key inds: for acoustic, 1 has click_n, 2 has left rate, 3 has right rate
% for electric, 1 has left pulse n, 2 right pulse n, 3 - 4 left and right
% rate

if any(strcmp(data.stimType,{'Acoustic','a'})) || length(data.keyInds) == 3
    left.stim = data.stimGrid(data.keyInds(2),:);
    right.stim = data.stimGrid(data.keyInds(3),:);
else
    left.stim = data.stimGrid(data.keyInds(3),:);
    right.stim = data.stimGrid(data.keyInds(4),:);
end

%% Detect sided data
% Adding new check for offStim on 2020-06-17: old RRTF data did NOT set the
% repetition rate to 0. Need to check if 'off' stim is valid and is needed,
% and then use the attenuation values to turn it to zero before passing to
% detectSidedData
% first term detects whether off data exists, second whether 
if ~any(round([left.stim, right.stim])==0) && any([left.stim - right.stim]~=0)
    tempInds = getKeyInds('IO',data.paramNames,data.fileName);
    leftAtt = data.stimGrid(tempInds(1),:);
    rightAtt = data.stimGrid(tempInds(2),:);
    left.stim(leftAtt==0|leftAtt==120) = 0;
    right.stim(rightAtt==0|rightAtt==120) = 0;
end
[ipsi, contra, bds] = detectSidedData(data,left,right,offset,offStim,1);

% Correction for bad data
ipsi.ipi = ipsi.stim(ipsi.stimIndices); ipsi.rates = 1000./ipsi.ipi;
contra.ipi = contra.stim(contra.stimIndices); contra.rates = 1000./contra.ipi;
bds.ipi = left.stim(bds.stimIndices); bds.rates = 1000./bds.ipi;

if strcmp(data.stimType,'e') && length(data.keyInds) == 4
    if strcmp(data.hemisphere,'l') % ips L, cont R
        ipsi.nPulses = unique(data.stimGrid(data.keyInds(1),ipsi.stimIndices));
        contra.nPulses = unique(data.stimGrid(data.keyInds(2),contra.stimIndices));
    else
        ipsi.nPulses = unique(data.stimGrid(data.keyInds(2),ipsi.stimIndices));
        contra.nPulses = unique(data.stimGrid(data.keyInds(1),contra.stimIndices));
    end
    allPulses = max(data.stimGrid(data.keyInds(1:2),:));
    bds.nPulses = allPulses(bds.stimIndices);
    if isfield(data,'itdC')
        [ipsi.itdC,contra.itdC,bds.itdC] = deal(data.itdC);
    end
elseif strcmp(data.stimType,'e') && length(data.keyInds) == 3
    if strcmp(data.hemisphere,'l') % ips L, cont R
        ipsi.nPulses = unique(data.stimGrid(data.keyInds(1),ipsi.stimIndices));
        contra.nPulses = unique(data.stimGrid(data.keyInds(1),contra.stimIndices));
    else
        ipsi.nPulses = unique(data.stimGrid(data.keyInds(1),ipsi.stimIndices));
        contra.nPulses = unique(data.stimGrid(data.keyInds(1),contra.stimIndices));
    end
    allPulses = data.stimGrid(data.keyInds(1),:);
    bds.nPulses = allPulses(bds.stimIndices);
    if isfield(data,'itdC')
        [ipsi.itdC,contra.itdC,bds.itdC] = deal(data.itdC);
    end
else
    ipsi.nPulses = unique(data.stimGrid(data.keyInds(1),ipsi.stimIndices));
    contra.nPulses = unique(data.stimGrid(data.keyInds(1),contra.stimIndices));
    bds.nPulses = unique(data.stimGrid(data.keyInds(1),bds.stimIndices));
end


%% Pack data for RRTF

ipsi = packData(ipsi,data.wdw,data.wdwMethod,data.itdC);
contra = packData(contra,data.wdw,data.wdwMethod,data.itdC);
bds = packData(bds,data.wdw,data.wdwMethod,data.itdC);

% visualize to see if worth continuing
repeatPlots(@rrtfRateVsSpikeCounts,contra,ipsi,bds,'rates','packDat','data');

cont = input('View Raster? Y(1)/N(0): ','s');

if any(regexpi(cont,'y')) || any(regexpi(cont,'1')) 
    repeatPlots(@rrtfRasterPlots,contra,ipsi,bds,'origDat')
end

cont = input('Continue? Y/N: ','s');

if any(regexpi(cont,'n')) || any(regexpi(cont,'0')) 
    status = 0;
    data.ipsi = ipsi;
    data.contra = contra;
    data.bds = bds;
else
    
    %% Data Analysis
    % This will look a lot like RRTF2018, although at this point, I do not
    % understand the motivation behind each VSA analysis used
    if ~isempty(contra.stimIndices)
        contra = vsaWrapper(contra,data);
    end
    if ~isempty(ipsi.stimIndices)
        ipsi = vsaWrapper(ipsi,data);
    end
    if ~isempty(bds.stimIndices)
        bds = vsaWrapper(bds,data);
    end
    
    %% Check output and decide whether to proceed
    repeatPlots(@rrtfSuccessRates,contra,ipsi,bds,'successRate')
    masterLegend = union(ipsi.rates,union(contra.rates,bds.rates));
    legend(num2str(round(masterLegend)'))
    
    cont = input('Continue? Y/N: ','s');
    
    if any(regexpi(cont,'n'))
        status = 0;
        data.ipsi = ipsi;
        data.contra = contra;
        data.bds = bds;
    else
        
        if ~isempty(ipsi.stimIndices) && sum(ipsi.summedSpkCnt)~=0
            ipsi = featureExtract(ipsi);
        else
            ipsi.lastRespInd = [];
        end
        if ~isempty(contra.stimIndices) && sum(contra.summedSpkCnt)~=0
            contra = featureExtract(contra);
        else
            contra.lastRespInd = [];
        end
        if ~isempty(bds.stimIndices) && sum(bds.summedSpkCnt)~=0
            bds = featureExtract(bds);
        else
            bds.lastRespInd = [];
        end
        
        if ~isempty(ipsi.stimIndices) && ~isempty(contra.stimIndices) & ~isempty(ipsi.lastRespInd) & ~isempty(contra.lastRespInd)
            dom = repRateDominance(contra,ipsi);
            % Plot results for acceptability
            figure; plot(dom.ipi,dom.dom)
            hold on
            plot(dom.unionIpi,dom.unionVals,'r')
            title('Rayleigh Dominance')
            legend({'Excess Comparison points','Union'})
        elseif isempty(ipsi.stimIndices)
            dom.ipi = [];
            dom.dom = 1;
            dom.wholeRangeMean = 1;
            dom.wholeRangeMedian = 1;
            dom.unionVals = 1;
            dom.unionIpi = [];
            dom.unionMean = 1;
            dom.unionMedian = 1;
            dom.rayStrLowCutoffDiff = Inf;
            dom.rayStrUpperCutoffDiff  = Inf;
        else
            dom.ipi = [];
            dom.dom = 1;
            dom.wholeRangeMean = -1;
            dom.wholeRangeMedian = -1;
            dom.unionVals = -1;
            dom.unionIpi = [];
            dom.unionMean = -1;
            dom.unionMedian = -1;
            dom.rayStrLowCutoffDiff = -Inf;
            dom.rayStrUpperCutoffDiff  = -Inf;
        end
        
        strStart = repmat({'filterCharacteristics.'},5,1);
        strEnd = [{'.data'};...
            {'.lowPassCutoff'};...
            {'.upperPassCutoff'};...
            {'.maxValueRate'};...
            {'.maxValue'}...
            ];
        
        figList = {'RayStrength','VS','vectLength','spikeRate','entrainment'};
        for ii = 1:length(figList)
            passArgs = [{'rates'};strcat(strStart,repmat(figList(ii),5,1),strEnd)];
            fig = repeatPlots(@rrtfFilterPlot,contra,ipsi,bds,passArgs{:});
            fig.Name = figList{ii};
        end
        
        
        %% Save and mark status
        cont = input('Continue? Y/N: ','s');
        if any(regexpi(cont,'n'))
            status = 0;
        else
            status = 1;
        end
        
        data.bds = bds;
        data.contra = contra;
        data.ipsi = ipsi;
        data.dom = dom;
        data.status = status;
        writeData(data)
        save([data.folder '\' data.fileName],'data')
        
    end

end

end

function writeData(data)
% Barf the data into an excel because this is easier than teaching people
% to code. 
% rm 2,3,4,5,7,8,9,10

cellTabs = unpackStruct2table(data);
cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}.sourceFiles = data.srcFile;
cellTabs(strcmp(cellfun(@(x) x.TableTitle,cellTabs),'bds'))=[];
cellTabs(strcmp(cellfun(@(x) x.TableTitle,cellTabs),'contra'))=[];
cellTabs(strcmp(cellfun(@(x) x.TableTitle,cellTabs),'ipsi'))=[];

cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')} = ...
    [cellTabs{strcmp(cellfun(@(x) x.TableTitle,cellTabs),'Meta')}...
    cell2table({input('Give a Comment: ','s')},...
    'VariableNames',{'Comment'})];

ii = 1;
while ii <= length(cellTabs)
    if numel(cellTabs{ii}) == 1
        cellTabs(ii) = [];
    else
        cellTabs{ii}.('Filename') = data.fileName;
        cellTabs{ii} = movevars(cellTabs{ii},'Filename','Before','TableTitle');
        ii = ii+1;
    end
end


newFold = fullfile(data.folder,data.fileName);
if ~exist(newFold,'dir')
    mkdir(newFold)
end

for ii = 1:numel(cellTabs)
    if exist([fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'],'file')
        delete([fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'])
    end
    writetable(cellTabs{ii},[fullfile(newFold,cellTabs{ii}.TableTitle{:}) '.xlsx'])
end

end

function [data] = repRateDominance(contra,ipsi)
% Performs integrative data analysis between two RRTF responses. Contra/stim
% convention used for clarity

% Figure out ranges to use for various dominance measures
% Union is defined in this case as the lowest threshold to the highest
% saturation
% Intersect is the highest threshold vs the lowest saturation

C = contra.filterCharacteristics.RayStrength.data;
I = ipsi.filterCharacteristics.RayStrength.data;
C(isnan(C))=0;
I(isnan(I))=0;

% Try to reduce data to common levels. Basically, we will find common
% levels and 'squeeze' both sets through the same restrictions.

% get matching values
matches = intersect(contra.ipi,ipsi.ipi);
% Get matching indices
[~,contraMatchInds] = intersect(contra.ipi,matches);
[~,ipsiMatchInds] = intersect(ipsi.ipi,matches);
% Squeeze contra levels
data.ipi = contra.ipi(contraMatchInds);
C = C(contraMatchInds);

%Squeeze ipsi levels
I = I(ipsiMatchInds);

%recheck
if sum(size(C) ~= size(I)) > 0
    error('TCs are not well matched!')
end


% General Dominance calculation
data.dom = (C - I) ./ (C + I);
data.dom(isnan(data.dom))=0;
data.wholeRangeMean = mean(data.dom);
data.wholeRangeMedian = median(data.dom);

% Union
unionEnd = max(contra.lastRespInd,ipsi.lastRespInd);
unionEnd = min(unionEnd,length(C));
data.unionVals = data.dom(1:unionEnd);
data.unionIpi = data.ipi(1:unionEnd);

data.unionMean = mean(data.unionVals);
data.unionMedian = median(data.unionVals);

% Filter differences
data.rayStrLowCutoffDiff = contra.filterCharacteristics.RayStrength.lowPassCutoff - ipsi.filterCharacteristics.RayStrength.lowPassCutoff;
data.rayStrUpperCutoffDiff = contra.filterCharacteristics.RayStrength.upperPassCutoff - ipsi.filterCharacteristics.RayStrength.upperPassCutoff;

end

function data = featureExtract(data)

% Get last valid stimRate (VS significance)
data.lastRespInd = find(data.noFirstPulse.PValue>0.01,1)-1; % I changed away from this, right? why?

if isempty(data.lastRespInd) && min(data.noFirstPulse.PValue)<0.01
    % no last resp ind because low pass cutoff was not reached
    data.lastRespInd = length(data.noFirstPulse.PValue);
    onsetOnly = arrayfun(@(x) cellfun(@(x) x(1),x),data.perPulseSpikes);
    [rho, pearsonPVal]=corr([...
        cellfun(@numel,onsetOnly(1:data.lastRespInd))',...
        cellfun(@mean,onsetOnly(1:data.lastRespInd))',...
        data.ipi(1:data.lastRespInd)']);
    
    corrVals = [rho(1,2),rho(1,3),rho(2,3)];
    corrPVals = [pearsonPVal(1,2),pearsonPVal(1,3),pearsonPVal(2,3)];
elseif ~isempty(data.lastRespInd)
    onsetOnly = arrayfun(@(x) cellfun(@(x) x(1),x),data.perPulseSpikes);
    [rho, pearsonPVal]=corr([...
        cellfun(@numel,onsetOnly(1:data.lastRespInd))',...
        cellfun(@mean,onsetOnly(1:data.lastRespInd))',...
        data.ipi(1:data.lastRespInd)']);
    
    corrVals = [rho(1,2),rho(1,3),rho(2,3)];
    corrPVals = [pearsonPVal(1,2),pearsonPVal(1,3),pearsonPVal(2,3)];
else
    [rho, pearsonPVal,corrVals,corrPVals] = deal(NaN);
end

data.filterCharacteristics.RayStrength = extractFilterChar(data.noFirstPulse,data.rates,'RayStrength');
data.filterCharacteristics.VS = extractFilterChar(data.noFirstPulse,data.rates,'VS');
data.filterCharacteristics.vectLength = extractFilterChar(data.noFirstPulse,data.rates,'vectLength');
data.filterCharacteristics.spikeRate = extractFilterChar(data.noFirstPulse,data.rates,'spikeRate');
data.filterCharacteristics.entrainment = extractFilterChar(data.noFirstPulse,data.rates,'entrainment');
data.filterCharacteristics.rates = data.rates;

% % two cases; either no last value under 0.01 found because all are under,
% % or none are under!
% if isempty(lastRespInd) && min(data.noFirstPulse.PValue)<0.01
%     lastRespInd = length(data.noFirstPulse.PValue);
%     
%     lastResp = data.ipi(lastRespInd);
%     
%     onsetOnly = arrayfun(@(x) cellfun(@(x) x(1),x),data.perPulseSpikes);
%         
%     [rho, pearsonPVal]=corr([cellfun(@numel,onsetOnly(1:lastRespInd))',cellfun(@mean,onsetOnly(1:lastRespInd))',...
%         data.ipi(1:lastRespInd)']);
%     
%     corrVals = [rho(1,2),rho(1,3),rho(2,3)];
%     corrPVals = [pearsonPVal(1,2),pearsonPVal(1,3),pearsonPVal(2,3)];
%     
%     % Filter Characteristics.
%     % Represent Rayleigh as filter function
%     ray = [data.noFirstPulse.RayStrength];
%     dbRay = 20*log2(ray);
%     dbRay = dbRay-max(dbRay(1:lastRespInd));
%     % To use the dbRay properly, Inf values are not permitted. Temporarily
%     % replace them with very negative numbers
%     dbRayNoINF = dbRay;
%     dbRayNoINF(dbRayNoINF==-Inf) = -100;
%     dbRayNoINF(isnan(dbRayNoINF)) = -100;
%     %Use log interpolation to find cutoff (-6 dB)
%     bestRate_RayStr = find(dbRay==0,1);
%     if bestRate_RayStr == 1
%         rayStrLowCutoffInd = NaN;
%         rayStrLowCutoff = NaN;
%         rayStrLowRolloff = Inf;
%     elseif all(dbRayNoINF(1:bestRate_RayStr-1)==-100)
%         % Indicates that interp wont be able to find points to interpolate from
%         rayStrLowCutoffInd = bestRate_RayStr;
%         rayStrLowCutoff = data.rates(bestRate_RayStr);
%         rayStrLowRolloff = 0;
%     else
%         rayStrLowCutoff = interp1(dbRayNoINF(1:bestRate_RayStr),data.rates(1:bestRate_RayStr),-6);
%         rayStrLowCutoffInd = interp1(dbRayNoINF(1:bestRate_RayStr),1:bestRate_RayStr,-6,'nearest');
%         if isnan(rayStrLowCutoffInd)
%             rayStrLowRolloff = 0;
%         else
%             rayStrLowRolloff = (dbRayNoINF(rayStrLowCutoffInd)-dbRayNoINF(1))/(data.rates(rayStrLowCutoffInd)-data.rates(1));
%         end
%     end
%     ind2use = min([lastRespInd+1 length(dbRayNoINF)]); % Added to control for cases where end of the array was reached
%     if bestRate_RayStr == ind2use % no upper rolloff can be found
%         rayStrUpperCutoff = NaN;
%         rayStrrayStrUpperCutoffInd = NaN;
%     elseif dbRayNoINF(bestRate_RayStr+1)==-100
%         % Indicates that interp wont be able to find points to interpolate from
%         rayStrrayStrUpperCutoffInd = bestRate_RayStr;
%         rayStrUpperCutoff = data.rates(bestRate_RayStr);
%         rayStrUpperRolloff = 0;
%     else
%         rayStrUpperCutoff =interp1(dbRayNoINF(bestRate_RayStr:ind2use),data.rates(bestRate_RayStr:ind2use),-6);
%         rayStrrayStrUpperCutoffInd = interp1(dbRayNoINF(bestRate_RayStr:ind2use),bestRate_RayStr:ind2use,-6,'nearest');
%     end
%     if isnan(rayStrrayStrUpperCutoffInd)
%         rayStrUpperRolloff = NaN;
%     else
%         rayStrUpperRolloff = (dbRay(lastRespInd)-dbRay(rayStrrayStrUpperCutoffInd))/(data.rates(rayStrrayStrUpperCutoffInd)-data.rates(lastRespInd));
%     end
%     rayStrbandWidth = rayStrUpperCutoff - rayStrLowCutoff; % If this gives a NaN, indicates that it is unknown
% 
% elseif (isempty(lastRespInd) && min(data.noFirstPulse.PValue)>0.01 )...
%         | lastRespInd == 0 | isnan(data.noFirstPulse.PValue(1:lastRespInd-1)) % Last case added for where most responses are invalid
%     lastRespInd = 1;
%     lastResp = NaN;
%     largestMean = NaN;
%     varMat = [NaN NaN; NaN NaN];
%     stationarity = NaN; 
%     ray = nan(1,length(data.noFirstPulse.PValue));
%     dbRay = ray;
%     bestRate_RayStr = NaN;
%     rayStrbandWidth = 0;
%     rayStrLowCutoff = NaN;
%     rayStrLowCutoffInd = NaN;
%     rayStrUpperCutoff = NaN;
%     rayStrrayStrUpperCutoffInd = NaN;
%     rayStrLowRolloff = NaN;
%     rayStrUpperRolloff = NaN;
%     rayStrLowCutoffVS = NaN;
%     rayStrLowCutoffIndVS = NaN;
%     rayStrUpperCutoffVS = NaN;
%     rayStrrayStrUpperCutoffIndVS = NaN;
%     rayStrLowRolloffVS = Inf;
%     rayStrUpperRolloffVS = Inf;
% elseif (isempty(lastRespInd) && min(data.noFirstPulse.PValue)>0.01 )...
%         | lastRespInd == 1
%     % Need at least two points of comparison for many of the following
%     % analyses
%     lastResp = data.ipi(lastRespInd);
%     largestMean = mean(data.noFirstPulse.meanLat,'omitnan');
%     varMat = [NaN NaN; NaN NaN];
%     stationarity = NaN;
%     ray = data.noFirstPulse.RayStrength;
%     dbRay = 20*log2(ray);
%     dbRay = dbRay-max(dbRay);
%     bestRate_RayStr = NaN;
%     rayStrbandWidth = 0;
%     rayStrLowCutoff = NaN;
%     rayStrLowCutoffInd = NaN;
%     rayStrUpperCutoff = NaN;
%     rayStrrayStrUpperCutoffInd = NaN;
%     rayStrLowRolloff = Inf;
%     rayStrUpperRolloff = Inf;
%     rayStrLowCutoffVS = NaN;
%     rayStrLowCutoffIndVS = NaN;
%     rayStrUpperCutoffVS = NaN;
%     rayStrrayStrUpperCutoffIndVS = NaN;
%     rayStrLowRolloffVS = Inf;
%     rayStrUpperRolloffVS = Inf;
% else
%     
%     lastResp = data.ipi(lastRespInd);
% 
%     % Check if onset responses are affected by repetition rate.
%     % Theoretically, up to the observable limit (in electric stimulation),
%     % the onset response should have stationary characteristics. If
%     % characteristics other than VS or mean phase change before the
%     % observation limit, then the repetition rate was too low and we should
%     % track when that occured. Unfortunately, there is no single metric to
%     % track this, so I will create an index to measure non-stationarity in
%     % onset response.
%     % Compared to earlier, will use pearson linear correlation, with pvalues,
%     % to compare the effects of mean latency, spike counts, and ipi in that
%     % order. corr outputs a 3x3 matrix when given a matrix of these three
%     % metrics, the first two on onset pulses only.
%     onsetOnly = arrayfun(@(x) cellfun(@(x) x(1),x),data.perPulseSpikes);
%     
%     % varMat = cov([data.onlyOnsetAllSpikes(1:lastRespInd).meanLatency],...
%     %     [data.onlyOnsetAllSpikes(1:lastRespInd).n]);
%     % varMat = cov(cellfun(@mean,onsetOnly(1:lastRespInd)),...
%     %     cellfun(@numel,onsetOnly(1:lastRespInd)));
%     %
%     % stationarity = sqrt(varMat(1,1)+varMat(2,2)+2*varMat(1,2));
%     
%     [rho, pearsonPVal]=corr([cellfun(@numel,onsetOnly(1:lastRespInd))',cellfun(@mean,onsetOnly(1:lastRespInd))',...
%         data.ipi(1:lastRespInd)']);
%     
%     corrVals = [rho(1,2),rho(1,3),rho(2,3)];
%     corrPVals = [pearsonPVal(1,2),pearsonPVal(1,3),pearsonPVal(2,3)];
%     
%     % Filter Characteristics.
%     % Represent Rayleigh as filter function
%     ray = [data.noFirstPulse.RayStrength];
%     ray(isnan(ray)) = 0;
%     dbRay = 20*log10(ray);
%     dbRay = dbRay-max(dbRay(1:lastRespInd));
%     % To use the dbRay properly, Inf values are not permitted. Temporarily
%     % replace them with very negative numbers
%     dbRayNoINF = dbRay;
%     dbRayNoINF(dbRayNoINF==-Inf) = -100;
%     dbRayNoINF(isnan(dbRayNoINF)) = -100;
%     %Use log interpolation to find cutoff (-6 dB)
%     bestRate_RayStr = find(dbRay==0,1);
%     rayStrLowCutoffInd = find(ray(bestRate_RayStr)/2-ray(1:bestRate_RayStr)>0,1);
%     if isempty(rayStrLowCutoffInd)
%         rayStrLowCutoffInd = NaN;
%         rayStrLowCutoff = NaN;
%         rayStrLowRolloff = Inf;
%     else
%         rayStrLowCutoffInd = interp1(ray(1:bestRate_RayStr),1:bestRate_RayStr,ray(bestRate_RayStr)/2,'nearest');
%         rayStrLowCutoff = interp1(ray(1:bestRate_RayStr),data.rates(1:bestRate_RayStr),ray(bestRate_RayStr)/2);
%         rayStrLowRolloff = (ray(bestRate_RayStr) - ray(rayStrLowCutoffInd))/(data.rates(bestRate_RayStr)-data.rates(rayStrLowCutoffInd));
%     end
%     rayStrrayStrUpperCutoffInd = find(ray(bestRate_RayStr)/2-ray(bestRate_RayStr:end)>0,1)+bestRate_RayStr-1;
%     if isempty(rayStrrayStrUpperCutoffInd)
%         rayStrrayStrUpperCutoffInd = NaN;
%         rayStrUpperCutoff = NaN;
%         rayStrUpperRolloff = Inf;
%     else
%         rayStrUpperCutoff = interp1(ray(bestRate_RayStr:rayStrrayStrUpperCutoffInd),data.rates(bestRate_RayStr:rayStrrayStrUpperCutoffInd),ray(bestRate_RayStr)/2);
%         rayStrUpperRolloff = (ray(bestRate_RayStr) - ray(rayStrrayStrUpperCutoffInd))/(data.rates(rayStrrayStrUpperCutoffInd)-data.rates(bestRate_RayStr));
%     end
%     
%     rayStrBandWidth = rayStrUpperCutoff - rayStrLowCutoff; % If this gives a NaN, indicates that it is unknown
% 
% end
% 
% %% VS FORM
% vs = data.noFirstPulse.VS;
% vs(vs==0) = NaN;
% rayStrrayStrUpperCutoffIndVS = find(vs<0.5,1);
% if isempty(rayStrrayStrUpperCutoffIndVS)
%     rayStrUpperCutoffVS = NaN;
%     rayStrUpperRolloffVS = Inf;
% else
%     rayStrUpperCutoff = interp1(vs(1:rayStrrayStrUpperCutoffIndVS),data.rates(1:rayStrrayStrUpperCutoffIndVS),0.5);
%     rayStrUpperRolloff = (vs(rayStrrayStrUpperCutoffIndVS-1) - vs(rayStrrayStrUpperCutoffIndVS))/(data.rates(rayStrrayStrUpperCutoffIndVS-1)-data.rates(rayStrrayStrUpperCutoffIndVS));
% end
% 
% %% Entrainment
% 
% 
% % This last command helps put everything created in the function workspace
% % into the data structure. You have to be super careful not to overwrite
% % anything though.
% clear dbRayNoINF ind2use
% data = ws2struct(data,'data');

end

function data = vsaWrapper(data,metaData)
% Preps data differently depending on analysis type at the moment
% Remember that incoming data is a cell vector, 1 x n of ipis, each ipi
% cell containg another cell array of n Pulses x n Sweeps, each of THESE
% cells containing all valid spike times for that pulse/sweep combination

%% Anon Funcs
extractSpikeTimes = @(x) [x{:}];
% VSA takes a list of spike times and an ipi as input
vsa = @(x,y) sqrt(sum(cos(2*pi.*x/y))^2 + sum(sin(2*pi.*x/y))^2)/length(x);
% mP calculates mean phase
mP = @(x,y) atan(sum(sin(2*pi.*x/y))^2/sum(cos(2*pi.*x/y))^2);

% Number of spikes per rep per ipi
data.rawSpkCnt = cellfun(@(x) cellfun(@numel,x,'UniformOutput',0),data.packDat,'UniformOutput',0);
% All spike latencies per ipi
data.allSpikes = arrayfun(@(x)  cellfun(@(x) extractSpikeTimes(x),x,'UniformOutput',0),data.packDat);
data.noFirstPulseDat = cellfun(@(x) x(2:end,:),data.packDat,'UniformOutput',0);
data.noFirstPulseSpikes = arrayfun(@(x)  cellfun(@(x) extractSpikeTimes(x),x,'UniformOutput',0),data.noFirstPulseDat);
data.noFirstPulseSpkCnt = cellfun(@numel,data.noFirstPulseSpikes);
% total number of responses per ipi
data.summedSpkCnt = cellfun(@numel,data.allSpikes);

% Analysis over all pulses
data.allSpike.VS = cellfun(@(x,y) vsa(x,y),data.allSpikes,num2cell(data.ipi));
data.allSpike.meanPhase = cellfun(@(x,y) mP(x,y),data.allSpikes,num2cell(data.ipi));%/(2*pi)*360; % in radians
data.allSpike.meanLat = cellfun(@mean,data.allSpikes);
data.allSpike.meanJitter = cellfun(@std,data.allSpikes);
data.allSpike.absPhaseLatencyDiff = data.allSpike.meanLat - data.allSpike.meanPhase/(2*pi).*data.ipi;
data.allSpike.vectPhaseLatencyDiff = 2*pi*data.allSpike.meanLat./data.ipi - data.allSpike.meanPhase;
% Significance of vector. This rejects the possibility of uniform,
% random distribution. Critical Z value obtained from Batchelet
% (book) 1965 and Mardia & Jupp 2000, but basically its the z (4.6052) value
% for a chi distribution, DOF = 2, see Rayleigh distribution for
% more information as to why
% Ok ignore previous part. Using approximation for p value from
% Jerrold H. Zar's book Biostatistical Analysis
data.allSpike.PValue = exp(sqrt(1+4*data.summedSpkCnt+...
    4*(data.summedSpkCnt.^2-(data.summedSpkCnt.*data.allSpike.VS).^2))-(1+2*data.summedSpkCnt));
data.allSpike.RayStrength = (data.summedSpkCnt.*data.allSpike.VS).^2 ./ data.summedSpkCnt;
%dispersion
data.allSpike.d1 = data.ipi./(2*pi).*sqrt(2*(1-data.allSpike.VS));
data.allSpike.d2 = data.ipi./(2*pi).*sqrt(-2*log(data.allSpike.VS));

% Skipping first pulse
data.noFirstPulse.VS = cellfun(@(x,y) vsa(x,y),data.noFirstPulseSpikes,num2cell(data.ipi));
data.noFirstPulse.VS(isnan(data.noFirstPulse.VS))=0;
data.noFirstPulse.meanPhase = cellfun(@(x,y) mP(x,y),data.noFirstPulseSpikes,num2cell(data.ipi));%/(2*pi)*360; % in radians
data.noFirstPulse.meanLat = cellfun(@mean,data.noFirstPulseSpikes);
data.noFirstPulse.meanJitter = cellfun(@std,data.noFirstPulseSpikes);
data.noFirstPulse.absPhaseLatencyDiff = data.noFirstPulse.meanLat - data.noFirstPulse.meanPhase/(2*pi).*data.ipi;
data.noFirstPulse.vectPhaseLatencyDiff = 2*pi*data.noFirstPulse.meanLat./data.ipi - data.noFirstPulse.meanPhase;
data.noFirstPulse.PValue = exp(sqrt(1+4*data.noFirstPulseSpkCnt+...
    4*(data.noFirstPulseSpkCnt.^2-(data.noFirstPulseSpkCnt.*data.noFirstPulse.VS).^2))-(1+2*data.noFirstPulseSpkCnt));
data.noFirstPulse.RayStrength = (data.summedSpkCnt.*data.noFirstPulse.VS).^2 ./ data.summedSpkCnt/size(data.data,1);
data.noFirstPulse.vectLength =data.noFirstPulse.RayStrength./data.noFirstPulse.VS;
data.noFirstPulse.d1 = data.ipi./(2*pi).*sqrt(2*(1-data.noFirstPulse.VS));
data.noFirstPulse.d2 = data.ipi./(2*pi).*sqrt(-2*log(data.noFirstPulse.VS));
data.noFirstPulse.count = data.noFirstPulseSpkCnt/metaData.nSweeps;
data.noFirstPulse.spikeRate = data.noFirstPulse.count/(metaData.sweepLength/1000);
data.noFirstPulse.entrainment = data.noFirstPulse.spikeRate./(data.nPulses-1);

% Analysis per pulse
% First command extracts all spikes per pulse per ipi
data.perPulseSpikes = cellfun(@(x) cellfun(@(x) extractSpikeTimes(x),num2cell(x,2),'UniformOutput',0),data.packDat,'UniformOutput',0);
% Prepare a cell of ipis to pass into vsa function
cellIpi = arrayfun(@(x,y) num2cell(repmat(x,y,1),2),data.ipi,cellfun(@length,data.perPulseSpikes),'UniformOutput',0);
% same as above but per pulse
data.perPulse.VS = cellfun(@(x,y) cellfun(@(x,y) vsa(x,y),x,y),data.perPulseSpikes,cellIpi,'UniformOutput',0);
data.perPulse.meanPhase = cellfun(@(x,y) cellfun(@(x,y) mP(x,y),x,y),data.perPulseSpikes,cellIpi,'UniformOutput',0);
data.perPulse.meanLat = cellfun(@(x) cellfun(@(x) mean(x,'omitnan'),x),data.perPulseSpikes,'UniformOutput',0);
data.perPulse.meanJitter = cellfun(@(x) cellfun(@(x) std(x,'omitnan'),x),data.perPulseSpikes,'UniformOutput',0);
data.perPulse.absPhaseLatencyDiff = cellfun(@(x,y,z) x - y/(2*pi)*z,data.perPulse.meanLat,data.perPulse.meanPhase,num2cell(data.ipi),'UniformOutput',0);
data.perPulse.vectPhaseLatencyDiff = cellfun(@(x,y,z) 2*pi*x./z - y,data.perPulse.meanLat,data.perPulse.meanPhase,num2cell(data.ipi),'UniformOutput',0);
% Counts valid spikes for each pulse/rep comination
data.allSpike.summedSpkCnt = arrayfun(@(x) cellfun(@(x) cellfun(@numel,x),x,'UniformOutput',0),data.perPulseSpikes);
% easier to write anon function for PValue at this point
pValFun = @(x,y) exp(sqrt(1+4*x+4*(x.^2-(x.*y).^2))-(1+2*x));
zValFun = @(x,y) (x.*y).^2 ./ x;
data.perPulse.PValue = cellfun(@(x,y) arrayfun(@(x,y) pValFun(x,y),x,y),data.allSpike.summedSpkCnt,data.perPulse.VS,'UniformOutput',0);
data.perPulse.RayStrength = cellfun(@(x,y) arrayfun(@(x,y) zValFun(x,y),x,y),data.allSpike.summedSpkCnt,data.perPulse.VS,'UniformOutput',0);
% normedVs is designed to show VS of non-first pulses as a strength
% relative to the first pulse, as VS is artificially lower for shorter IPIs
firstPulseVS = cellfun(@(x) x(1),data.perPulse.VS);
cellVS = arrayfun(@(x,y) repmat(x,y,1),firstPulseVS,cellfun(@length,data.perPulseSpikes),'UniformOutput',0);
data.normedVS = cellfun(@(x,y) x./y,data.perPulse.VS,cellVS,'UniformOutput',0);

% find first non-sig VS pulse
nonOnsetPValue=cellfun(@(x) x(2),data.perPulse.PValue);
if find(nonOnsetPValue>0.05|isnan(nonOnsetPValue),1) ~= 1
    data.lastSuccessfulFollow = data.ipi(find(nonOnsetPValue>0.05|isnan(nonOnsetPValue),1)-1);
else
    data.lastSuccessfulFollow = 1;
end
% Find first pulse at each rate that fails to follow
data.firstFailedPulse = cellfun(@(x) find(x>0.05|isnan(x),1),  data.perPulse.PValue,  'UniformOutput',  false);
% Convert to non-cell
data.firstFailedPulse(cellfun(@isempty,data.firstFailedPulse))={0};
data.firstFailedPulse=num2cell(data.firstFailedPulse);
% Failure rate per pulse
data.successRate = cellfun(@(x) sum(cellfun(@(x) x~=0,x),2)/20, data.rawSpkCnt,'UniformOutput',0);
data.failureRate = cellfun(@(x) 1-x,data.successRate,'UniformOutput',0);

errFun = @(x,y) NaN;

data.firstSpikeLatency = cellfun(@(x) cellfun(@(x)...
    x(1),x,'ErrorHandler',errFun),data.packDat,'UniformOutput',0);
data.firstSpike.perPulseMean = cellfun(@(x) mean(x,2,'omitnan'),data.firstSpikeLatency,'UniformOutput',false);
data.firstSpike.perRateMean = cellfun(@(x) mean(x,'all','omitnan'),data.firstSpikeLatency,'UniformOutput',false);
data.firstSpike.perPulseJitter = cellfun(@(x) std(x,2,'omitnan'),data.firstSpikeLatency,'UniformOutput',false,'ErrorHandler',errFun);
data.firstSpike.perRateJitter = cellfun(@(x) std(x,'all','omitnan'),data.firstSpikeLatency,'UniformOutput',false,'ErrorHandler',errFun);

end

function data = packData(data,wdw,wdwMethod,itdC)

if strcmp(wdwMethod,'constant')
    data.rates = 1000./data.ipi;
    if isfield(data,'itdC')
        maxRate = max(wdw-data.itdC);
    else
        maxRate = 0;
    end
    for ii = 1:min([length(data.ipi) length(data.nPulses)])
        if data.ipi(ii)>maxRate
            [data.packDat{ii},data.origDat{ii}] = ...
                wdwFunc(data.data(:,ii),wdw,data.nPulses(ii),data.ipi(ii));
        else
            [data.packDat{ii},data.origDat{ii}] = ...
                wdwFunc(cell(size(data.data(:,ii))),wdw,data.nPulses(ii),data.ipi(ii));
        end
    end
else
    for ii = 1:min([length(data.ipi) length(data.nPulses)])
        shrunkWdw(1) = wdw(1);
        shrunkWdw(2) = data.ipi(ii)-wdw(2);
        shrunkWdw = shrunkWdw + itdC;
        [data.packDat{ii},data.origDat{ii}] = ...
            wdwFunc(data.data(:,ii),shrunkWdw,data.nPulses(ii),data.ipi(ii));
    end
end
end

function data = dominance(contra,ipsi)

%% Fit Values
x = 0:0.01:100;

C = contra.vals(x);
C(x<floor(contra.threshold))=0;
I = ipsi.vals(x);
I(x<floor(ipsi.threshold))=0;
absThr = min(contra.threshold,ipsi.threshold);
absSat = max(contra.saturation,ipsi.saturation);

data.dom = (C-I) ./ (C+I);
data.dom(isnan(data.dom))=0;
data.domVal = mean(data.dom(x>absThr&x<absSat));
data.domVal70 = data.dom(x==70);
data.domVal80 = data.dom(x==80);
midRange = round((absSat+absThr)/2,2);
data.domValMidRange = data.dom(find(x-midRange<0.01&x-midRange>-0.01,1));

data.domMed = median(data.dom(x>absThr&x<absSat));

figure; plot(x,data.dom)
hold on
plot([absThr absSat],[data.domVal data.domVal])
plot([absThr absSat],[data.domMed data.domMed])
scatter(70,data.domVal70)
scatter(80,data.domVal80)
scatter(round((absSat+absThr)/2,2),data.domValMidRange)
hold off

end


function data = respParams(data,currentPlot)
invertedSig = @(y, vals) vals(3)/4 * erfinv((y-vals(4))/vals(1)) + vals(2);

vals = coeffvalues(data.vals);

data.saturationSpikeRate = vals(1)+vals(4);

data.threshold = invertedSig(data.saturationSpikeRate*0.05,vals);
data.saturation = invertedSig(data.saturationSpikeRate*0.95,vals);
data.dynamicRange = data.saturation - data.threshold;

hold(currentPlot);
scatter(currentPlot,data.threshold,data.saturationSpikeRate*0.05,'*')
scatter(currentPlot,data.saturation,data.saturationSpikeRate*0.95,'o')
hold off

%% Latency
fixEmpty = cellfun(@isempty,data.data);
latMap = data.data; latMap(fixEmpty) = {NaN};
latMap = cellfun(@(x) x(1), latMap);
data.firstLat = squeeze(mean(latMap,[1 2],'omitnan'));
data.firstLatStd = squeeze(std(latMap,0,[1 2],'omitnan'));
data.thresholdLatency = interp1(data.rates,data.firstLat,data.threshold);
data.saturationLatency = interp1(data.rates,data.firstLat,data.saturation);
data.thresholdLatencyStd = interp1(data.rates,data.firstLatStd,data.threshold);
data.saturationLatencyStd = interp1(data.rates,data.firstLatStd,data.saturation);

end







