function [axHandle] = rrtfFilterPlot(axHandle, rates, featureDat,...
    lowerCutoff, upperCutoff,maxRate,maxVal)
%TCCONTOURS Takes incoming axes handle and plots the relevant X-Y
%information on a contour plot of the tuning curve desired.

plot(rates,featureDat)
curLim = ylim;
ylim([0,curLim(2)])
hold on
if isnan(upperCutoff)
    uC = 0;
else
    uC = upperCutoff;
end
if isnan(lowerCutoff)
    lc = rates(end);
else
    lc = lowerCutoff;
end
plot([uC lc],[1 1]*maxVal/2,'g--')
scatter(maxRate,maxVal)

legend({'','Lower/Upper Cutoff','Best Rate'},'Location','south')
hold off

end

