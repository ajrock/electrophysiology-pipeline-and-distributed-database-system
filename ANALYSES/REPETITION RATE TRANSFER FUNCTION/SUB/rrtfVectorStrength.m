function [axHandle] = rrtfVectorStrength(axHandle, rates, noFirstPulse, lowerCutoffVS, upperCutoffVS)
%TCCONTOURS Takes incoming axes handle and plots the relevant X-Y
%information on a contour plot of the tuning curve desired.

plot(rates,noFirstPulse.VS)
hold on
ylim([0 1])
if isempty(lowerCutoffVS)||isnan(lowerCutoffVS)
    lC = 0;
else
    lC = lowerCutoffVS;
end
if isempty(upperCutoffVS)||isnan(upperCutoffVS)
    upperCutoffVS = 0;
end
plot([lC upperCutoffVS],[0.5 0.5],'g--')
legend({'VS Values','Lower/Upper Cutoff'},'Location','south')
hold off

end

