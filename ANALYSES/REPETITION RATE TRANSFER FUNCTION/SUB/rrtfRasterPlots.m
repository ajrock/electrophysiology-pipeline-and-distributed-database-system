function [axHandle] = rrtfRasterPlots(axHandle, data)
%TCCONTOURS Takes incoming axes handle and plots the relevant X-Y
%information on a contour plot of the tuning curve desired.

hold on
for ii = 1:length(data)
    for jj = 1:size(data{ii},2)
        plotThis = [data{ii}{:,jj}];
        scatter(plotThis,ones(length(plotThis),1)*jj+size(data{ii},2)*(ii-1),'r.')
    end
    if rem(ii,2)==1
        sz = size(data{ii},2);
        patch([0 0 600 600], sz*[(ii-1) (ii) (ii) (ii-1)],[.5 .5 .5],'FaceAlpha',.5)
    end
end
hold off

end

