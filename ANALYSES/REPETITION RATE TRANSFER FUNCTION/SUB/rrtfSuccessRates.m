function [axHandle] = rrtfSuccessRates(axHandle, successRate)
%TCCONTOURS Takes incoming axes handle and plots the relevant X-Y
%information on a contour plot of the tuning curve desired.

hold on
% plot Length is because I wanted to plot first five pulses, but
% some super low rates went below that.
plotLength = min(cellfun(@length,successRate),5);
sR = cellfun(@(x) x(1:plotLength),successRate,'UniformOutput',false);
cellfun(@(x) plot(x),sR);
hold off

end

