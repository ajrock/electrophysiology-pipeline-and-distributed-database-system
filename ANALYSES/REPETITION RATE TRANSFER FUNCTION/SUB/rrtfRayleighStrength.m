function [axHandle] = rrtfRayleighStrength(axHandle, rates, dbRay, lowerCutoff, upperCutoff)
%TCCONTOURS Takes incoming axes handle and plots the relevant X-Y
%information on a contour plot of the tuning curve desired.

plot(rates,dbRay)
hold on
curLim = ylim;
ylim([curLim(1) 10]);
if isnan(lowerCutoff)
    lC = 0;
else
    lC = lowerCutoff;
end
plot([lC upperCutoff],[0 0],'g--')
legend({'Rayleigh Values','Lower/Upper Cutoff'},'Location','south')
hold off

end

