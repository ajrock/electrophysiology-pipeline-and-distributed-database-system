function data = determineWindowMethod(data)
%DETERMINEWINDOWMETHOD Summary of this function goes here
%   Detailed explanation goes here
validResp = 0;
while validResp == 0
    fprintf(['Choose a windowing method: \n',...
        'Constant: will apply your window at a consistent,\n',...
        '    specific latency window\n',...
        '    following the stimulus.\n',...
        'Shrink: the window identifies time regions \n',...
        '    around each period to blank,\n',...
        '    and reduces window size for each increasing stimulus rate.\n'])
    wdwMethod = input('1:(c)onstant or 2:(s)hrink: ','s');
    
    if any(contains(wdwMethod,{'1','c'}))
        wdwMethod = 'constant';
    end
    if any(contains(wdwMethod,{'2','s'}))
        wdwMethod = 'shrink';
    end
    
    if ~any(strcmp(wdwMethod,{'constant','shrink'}))
        disp('Invalid Input!')
    else
        validResp = 1;
    end
    
end
if strcmp(wdwMethod,'shrink')
    % Apply shrink edges
    data.wdw(1) = data.wdw(1)-data.itdC;
    data.wdw(2) = data.repRate - data.wdw(2) + data.itdC;
end
data.wdwMethod = wdwMethod;
end

