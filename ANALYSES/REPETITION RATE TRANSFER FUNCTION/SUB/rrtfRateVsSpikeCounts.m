function [axHandle] = rrtfRateVsSpikeCounts(axHandle, rates, packDat, data)
%TCCONTOURS Takes incoming axes handle and plots the relevant X-Y
%information on a contour plot of the tuning curve desired.
extractSpikeTimes = @(x) [x{:}];

plot(rates(1:length(packDat)),cellfun(@numel,arrayfun(@(x)...
        cellfun(@(x) extractSpikeTimes(x),x,'UniformOutput',0),...
        packDat))/size(data,1))

% [X, Y] =  meshgrid(freqs,dB);
% contourf(axHandle,X,Y,data,5);
% set(axHandle,'xscale','log');
% axHandle.XTick=[500 1000 2000 4000 8000 16000];
% axHandle.XTickLabel={'0.5','1','2','4','8','16'};

end

