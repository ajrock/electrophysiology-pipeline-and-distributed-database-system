function [filtChars] = extractFilterChar(data,rates,feature)
%extractFilterChar Runs the data processing to get rate at max value, 50% cutoff
%and halfbandwidth


featDat = data.(feature);

% Max Values

[maxValue,maxValueInd]=max(featDat);

maxValueRate = getPeakResponse(featDat,maxValue,rates);

% Lowpass Cutoff
lowPassCutoff = getLowPass(featDat,maxValueInd,rates);

upperPassCutoff = getUpperPass(featDat,maxValueInd,rates);

%Bandwidth
bandwidth = lowPassCutoff-upperPassCutoff;

filtChars.maxValue = maxValue;
filtChars.maxValueRate = maxValueRate;
filtChars.lowPassCutoff = lowPassCutoff;
filtChars.upperPassCutoff = upperPassCutoff;
filtChars.bandwidth = bandwidth;
filtChars.data = featDat;


end

function lowPassCutoff = getLowPass(dat,maxRateInd,rates)

lastPosValInd = find(dat(maxRateInd)/2 - dat(maxRateInd:end)>0,1)-1;
if isempty(lastPosValInd)
    lowPassCutoff  = NaN;%rates(end);
else
    datPoints=dat(maxRateInd +lastPosValInd-[1 0]);
    ratePoints=rates(maxRateInd +lastPosValInd-[1 0]);
    if ~any(isnan([datPoints ratePoints]))
        lowPassCutoff  = interp1(datPoints,ratePoints,dat(maxRateInd)/2);
    end
end

end

function upperPassCutoff = getUpperPass(dat,maxRateInd,rates)


lastPosValInd = find(dat(maxRateInd)/2 - dat(1:maxRateInd)<0,1);
if isempty(lastPosValInd) || lastPosValInd == 1
    upperPassCutoff  = NaN;
else
    datPoints=dat(lastPosValInd-[1 0]);
    ratePoints=rates(lastPosValInd-[1 0]);
    if ~any(isnan([datPoints ratePoints]))
        upperPassCutoff  = interp1(datPoints,ratePoints,dat(maxRateInd)/2);
    end
end


end

function peakResponse = getPeakResponse(dat,maxVal,rates)

lastPosValInd = find((maxVal*0.99999 - dat)<0,1,'last');
if isempty(lastPosValInd)
    peakResponse = NaN;
else
    peakResponse = rates(lastPosValInd);
end

end

function [A,B] = errFun(S,varargin)
    %warning(S.identifier, S.message); 
    A = NaN; 
    B = NaN;
end
