function analyzeAllAnimals(inputFolder)
%analyzeAllAnimals Summary of this function goes here
%   Detailed explanation goes here

if ~exist('inputFolder','var')
    inputFolder = uigetdir('..');%'cd);
end
d = dir([inputFolder '\20*']);

for ii = 1:length(d)
    sprintf('Now Analyzing %s, which is %d of %d animals',...
        d(ii).name,...
        ii,...
        length(d))
    analyzeAnimal([d(ii).folder '\' d(ii).name])
end

end

