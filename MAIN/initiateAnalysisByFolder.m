function initiateAnalysisByFolder(inputFolder)
%initiateAnalysisByFolder Summary of this function goes here
%   Detailed explanation goes here
close all
if nargin == 0
    inputFolder = uigetdir(cd);
end

fileType = regexp(inputFolder,'\');
if isempty(fileType)
    error('Bad Folder Selection')
end
fileType = inputFolder(fileType(end)+1:end);

matFile = checkFolderContents(inputFolder);

toBeDone = find(contains(cellstr(matFile.analysisDone),'no'))';
select = [];
while ~isempty(toBeDone)
    ii = toBeDone(1);
    fileName = matFile.fileName{ii};
    disp(fileName)
    if exist([inputFolder '\' fileName])
    [data,psthFig(1)] = readSRC([inputFolder '\' fileName]);
    data.srcFile{1} = fileName;
    psthFig(1).Name = 'PeriodicTimeHistogram';
    title(sprintf(matFile.fileName{ii}(1:end-4)),'Interpreter','none')
    if ~isempty(data.stimGrid)
    data.keyInds = getKeyInds(fileType,data.paramNames,fileName);
    % check for fractured data
    if any(strcmp(fileType,{'TC','IO','RRTF'}))
        % Ask for files, ID files, and add them to data while also removing
        % them from from the srclist for consideration
        resp = input('Do you want to append other src files? Y/N: ','s');
        if any(regexpi(resp,'y')) || strcmp(resp,'1')
            shortList = matFile.fileName(toBeDone);
            [radFig] = radioSelection(shortList);
            select = guidata(radFig);
            delete(radFig)
            if isempty(select)
                select = 'Not Used';
            else
                track = 2;
                for getData = select
                    [addData,psthFig(track)] = readSRC([inputFolder '\' getData{:} '.src']);
                    data.srcFile{track} = getData{:};
                    psthFig(track).Name = sprintf('%s_%s',psthFig(1).Name,getData{1});
                    title(sprintf(getData{:}))
                    if strcmp(fileType,'RRTF') % RRTF not transformed like other data
                        if isempty(addData.spikeMat)
                            skipFlag = 1;
                            warning('No spike mat! Should probably skip the whole measure')
                        else
                            skipFlag = 0;
                        end
                        if size(data.spikeMat,1)==size(addData.spikeMat,1)
                            data.spikeMat = cat(2,data.spikeMat, addData.spikeMat);
                        else
                            [~,takeThis]=max([size(data.spikeMat,1),size(addData.spikeMat,1)]);
                            if takeThis ~= 1
                                data.spikeMat = addData.spikeMat;
                                stimGridFlag = 1;
                            else
                                stimGridFlag = 0;
                            end
                        end
                        if ~skipFlag && addData.wdw(1) ~=0
                            data.wdw(1) = min(data.wdw(1),addData.wdw(1));
                        end
                        if ~skipFlag
                            data.wdw(2) = max(data.wdw(2),addData.wdw(2));
                        end
                    else
                        if ~isempty(data.spikeMat)
                            catSize = min([size(data.spikeMat,2),size(addData.spikeMat,2)]);
                            if catSize~=0
                                data.spikeMat = cat(3,data.spikeMat(:,1:catSize,:), addData.spikeMat(:,1:catSize,:));
                            end
                        else
                            catSize = size(addData.spikeMat,2);
                            data.spikeMat = addData.spikeMat(:,1:catSize,:);
                            data.stimGrid = addData.stimGrid;
                            data.fullMat = addData.fullMat;
                            catSize = 0;
                        end
                    end
                    if ~exist('catSize') || catSize ~=0
                        if exist('stimGridFlag')
                            if stimGridFlag
                                data.stimGrid = addData.stimGrid;
                            end
                        else
                            data.stimGrid = [data.stimGrid addData.stimGrid];
                            data.fullMat(track) = addData.fullMat;
                        end
                    end
                    track = track + 1;
                end
            end
        else
            select = 'Not Used';
        end
    else
        select = 'Not Used';
    end
    data.fileName = fileName(1:end-4);
    data.folder = inputFolder;
    if ~isempty(data.spikeMat) || (exist('skipFlag') && ~skipFlag)
        switch fileType
            case 'IO'
                status = ioAnalysis(data);
            case 'ITD'
                status = itdAnalysis(data);
            case 'RRTF'
                status = rrtfAnalysis(data);
            case 'TC'
                status = tcAnalysis(data);
            otherwise
                error('Bad Folder Selection! Folder must contain either IO, ITD, RRTF, or TC, ONLY!')
        end
    else
        status = 0;
    end
    else
        status = 0;
        select = 'Not Used';
    end
    else
        status = 0;
        select = 'Not Found';
        fprintf('File %s not found!\n',fileName)
    end

    
    if status == 1
        matFile.analysisUsed(toBeDone(1)) = {'yes'};
        shouldFigsBeSaved = input('Save Figures? y/Y/1: ','s');
    else
        matFile.analysisUsed(toBeDone(1)) = {'no'};
        shouldFigsBeSaved = 'n';
    end
    
    if contains(shouldFigsBeSaved,{'y','Y','1'})
        newFold = fullfile(data.folder,data.fileName);
        if ~exist(newFold,'dir')
            mkdir(newFold)
        end
        saveAllFigs(newFold)
    end
    close all
    select = find(contains(matFile.fileName(toBeDone),select));
    matFile.analysisUsed(toBeDone(select)) = deal({'no'});
    select = [select; 1];
    matFile.analysisDone(toBeDone(select)) = deal({'yes'});
    toBeDone(select) = [];
    select = [];
    save([inputFolder '\folderData'],'matFile')
    close all
end

end

