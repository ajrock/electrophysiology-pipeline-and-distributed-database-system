function analyzeAnimal(inputFolder)
%analyzeAnimal shortcut for beginning analysis of all aspects for one
%animal
%   Detailed explanation goes here
if nargin == 0
    inputFolder = uigetdir(cd);
end
initiateAnalysisByFolder([inputFolder '\IO'])
initiateAnalysisByFolder([inputFolder '\ITD'])
initiateAnalysisByFolder([inputFolder '\TC'])
initiateAnalysisByFolder([inputFolder '\RRTF'])

end

