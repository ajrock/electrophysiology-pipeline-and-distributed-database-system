function [psth,psthFig,spikeMat,wdw] = windowDecisionLoop(spikeMat,nPulses,repRate,sweepLength,rrtfFlag,itdC)
%[psth,psthFig] = windowDecisionLoop(fileName, fileType) Summary of this function goes here
%   Detailed explanation goes here

% Make first PSTH
if nPulses == 1
    [psth,psthFig] = makePSTH(spikeMat,sweepLength,'nPulses',num2str(nPulses),'repRate',num2str(repRate));
elseif rrtfFlag ==1
    [psth,psthFig] = makePSTH(spikeMat(:,1),ceil(repRate)*2,'nPulses',num2str(nPulses),'repRate',num2str(repRate));
else
    [psth,psthFig] = makePSTH(spikeMat,ceil(repRate)*2,'nPulses',num2str(nPulses),'repRate',num2str(repRate));
end

% First latency detection

[spon, wdw, psthFig] = detectResponseEdges(psth,psthFig,[],[]);
if ~isempty(itdC)
    hold on
    plot(psthFig.Children,[1 1]*itdC,ylim(psthFig.Children),'r-')
    hold off
end

decision = 0;
while decision ~= 1
    wdw
    fprintf(['Choose an Option: \n'...
        '1: continue with these latencies \n'...
        '2: Update detection parameters \n'...
        '3: Set latencies manually \n'])
    if rrtfFlag
       fprintf(['For RRTFs, if you wish to apply the SHRINK window method, \n',...
           'please set the window to cover as much of the period between stimulations ',...
           'as possible \n'])
    end
    decision = input(' ');
    if isempty(decision) || ~isnumeric(decision) || decision < 1 || decision > 3
        %warndlg('Numbers between 1 and 3 only you pitiful excuse of a human being!')
        decision = 4;
    end
    
    switch decision
        case 2
            fprintf('Select desired threshold on figure: ')
            [~, thresh] = ginput(1);
            gap = input('Select subtrigger gap: ');
            close(psthFig)
            if nPulses == 1
                [psth,psthFig] = makePSTH(spikeMat,sweepLength,'nPulses',num2str(nPulses),'repRate',num2str(repRate));
            elseif rrtfFlag ==1
                [psth,psthFig] = makePSTH(spikeMat,50,'nPulses','1','repRate',num2str(repRate));
            else
                [psth,psthFig] = makePSTH(spikeMat,ceil(repRate),'nPulses',num2str(nPulses),'repRate',num2str(repRate));
            end
            if ~isempty(itdC)
                hold on
                plot(psthFig.Children,[1 1]*itdC,ylim(psthFig.Children),'r-')
                hold off
            end
            [spon, wdw, psthFig] = detectResponseEdges(psth,psthFig,thresh,gap);
        case 3
            wdw(1) = input('Select first latency: ');
            wdw(2) = input('Select second latency: ');
            if nPulses == 1
                [psth,psthFig] = makePSTH(spikeMat,sweepLength,'nPulses',num2str(nPulses),'repRate',num2str(repRate));
            elseif rrtfFlag ==1
                [psth,psthFig] = makePSTH(spikeMat(:,1),round(repRate),'nPulses',num2str(nPulses),'repRate',num2str(repRate));
            else
                [psth,psthFig] = makePSTH(spikeMat,ceil(repRate)*2,'nPulses',num2str(nPulses),'repRate',num2str(repRate));
            end
            hold on
            histogram(psth.Data(find(psth.Data>wdw(1) & psth.Data<wdw(2))),'BinWidth',1,'FaceColor','yellow');
            if ~isempty(itdC)
                plot(psthFig.Children,[1 1]*itdC,ylim(psthFig.Children),'r-')
            end
            hold off
    end
    
end

if rrtfFlag == 0 % Do not window yet for RRTF
    spikeMat = wdwFunc(spikeMat,wdw,nPulses,repRate);
end

end