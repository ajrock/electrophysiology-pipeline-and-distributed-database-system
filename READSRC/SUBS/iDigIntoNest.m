function [var] = iDigIntoNest(data,var2find)
%iDigIntoNest removes a vector of data from given table based on substructe
%names
% Input: data, or table with desired data in it. var2find, or a char array
% of the sequence needed to access this parameter
%   Based on number of dots(.) in the variable defined, it extracts
%   information level by level. Matlab sometimes has trouble doing this one
%   its own. Commonly used when perusing tables of data for relevant
%   variables to examine. Keep in mind that the entire 'pathway' to the
%   final variable must be explicitly said!
%   2019-01-24 AC: added ability to change to cell output if final variable
%   has varying lengths of output (matrix becomes impossible) 
%   2019-01-28 AC: Caught bug that caused program to try to calculate two
%   fields at once if more than one is found

structSep = strfind(var2find,'.');
if ~isempty(structSep)
    var = data;
    for i = 1:length(structSep)
        structName = var2find(1:structSep(i)-1);
        if i ~=length(structSep) 
            structField = var2find(structSep(i)+1:structSep(i+1)-1);
            var = [var.(structName).(structField)];
        elseif length(structSep) == 1
            structField = var2find(structSep(i)+1:end);
            if length(unique(cellfun(@length, {var.(structName).(structField)})))==1
                var = [var.(structName).(structField)];
            else
                var = {var.(structName).(structField)};
            end
        else
            structField = var2find(structSep(i)+1:end);
            var = [var.(structField)];
        end
    end
    %      structName = var2find(1:structSep-1);
    %      structField = var2find(structSep+1:end);
    %      var = [data.(structName).(structField)];
else
    var = [data.(var2find)];
end

end

