function [psth,psthFig] = makePSTH(data,sweepLength,varargin)
%makePSTH very simple function that creates a Peri-stimulus-time-histogram
%from a cell matrix of spike timings. May expand capabilities later.
% OPTIONAL INPUT: holdoutDim, if you want a separate histogram for each
% entry along a certain dimension. Integer dimension value (i.e, 1)

psthFig = figure;
if contains(varargin,'holdoutDim')
    shiftDat = shiftdim(data,varargin(find(contains(varargin,'holdoutDim'))+1)-1);
    figure;
    for ii = 1:length(shiftDat,1)
        subplot(length(shiftDat),1,ii);
        psth(ii) = histogram([shiftDat{ii,:}],'BinWidth',1);
    end
elseif any(contains(varargin,'nPulses')) & any(contains(varargin,'repRate'))
    nPulses = str2num(varargin{find(contains(varargin,'nPulses'))+1});
    repRate = str2num(varargin{find(contains(varargin,'repRate'))+1});
    if nPulses == 1 | repRate == 0
        psTransform = [data{:}];
    else
        psTransform = [];
        dataVect = [data{:}];
        for pulse=1:nPulses
            psTransform = [psTransform dataVect(dataVect<sweepLength & dataVect>0)];
            %shift bin
            dataVect = dataVect - repRate;
        end
    end
    psth = histogram(psTransform,0:1:sweepLength);
else
    psth = histogram([data{:}],0:1:sweepLengt);
end
title('PeriodicTimeHistogram: All Sweeps');
xlabel('Time Since Cycle Onset to 2 Cycles [ms]');
ylabel('Summed Spike Count per [ms] bin');


end

