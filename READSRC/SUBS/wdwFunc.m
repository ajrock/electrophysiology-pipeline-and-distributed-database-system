function [spikeMat,origWdwMat] = wdwFunc(data,wdw,nWdws,period)
%[spikeMat] = wdwFunc(data,wdw,nWdws) 
%   takes in a cell array of spike times and reduces the count of times to
%   only those within the valid wdw values given. Expected input is of form
%   rep x var1 x var2... i.e., for a 6 pulse, 12 rep ITD between
%   abs 2ms in 50 us steps, size would be 12 * 81. If multiple stim
%   levels used, would be 12 * 81 * #... output adds the pulse dimension.
%   wdw is a 2 element matrix with a 'start' and 'stop' value.
%   nWdws and period are optional inputs for when there are multiple
%   windows. Data is however transformed to give spike times corresponding
%   to stimulus onsets.

finalSize = [nWdws, size(data)];
anonWin = @(x,y) x(x>y(1)&x<y(2));
%spikeMat = cell(finalSize);

for ii = 1:nWdws
    thisPulse = cellfun(@(x) x-period*(ii-1),data,'UniformOutput',false);
    thisPulseWindowed = cellfun(@(x) anonWin(x,wdw),thisPulse,'UniformOutput',0);
    holdingMat{ii} = thisPulseWindowed;
    if nargout == 2
        holdWdwMat{ii} = cellfun(@(x) x+period*(ii-1),thisPulseWindowed,'UniformOutput',0);
    end
end
spikeMat = holdingMat{1};
for ii = 2:nWdws
    spikeMat = cat(3,spikeMat,holdingMat{ii});
end
spikeMat = permute(spikeMat,[3 1 2]);

if nargout == 2
    origWdwMat = holdWdwMat{1};
    for ii = 2:nWdws
        origWdwMat = cat(3,origWdwMat,holdWdwMat{ii});
    end
    origWdwMat = permute(origWdwMat,[3 1 2]);
end


end