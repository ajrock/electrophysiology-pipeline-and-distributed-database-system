function [data,wdw,psthFig] = detectResponseEdges(psth,psthFig,thresh,gap)
%detectResponseEdges Summary of this function goes here
%   Detailed explanation goes here

data.spon = psth.Values(1:10);
data.sponMean = mean(data.spon);
data.sponStd = std(data.spon);
vals = psth.Values(18:end);

if isempty(gap)
    gap = 2;
end
if isempty(thresh)
    thresh = 3;

    sponUsed = max([data.sponMean .00001]);
    sponTrigger = sponUsed + thresh*data.sponStd;
else
    sponTrigger = thresh;
end

[maxInd, data.max] = max(vals);

subTriggerPoints = find(vals>sponTrigger);
subTriggerGaps = diff(subTriggerPoints);
if isempty(subTriggerGaps)
   subTriggerGaps = 1; 
end

% subtrigger gap of max 1 allowed
firstLat = subTriggerPoints(find(subTriggerGaps(subTriggerPoints<data.max)<=2,1))-1;
if isempty(firstLat)
    firstLat = 0;
end

maxInd = find(subTriggerPoints==data.max);
subTriggerGaps(1:maxInd-1) = [];
subTriggerPoints(1:maxInd-1) = [];
if isempty(subTriggerPoints)
    subTriggerPoints = 1;
end
% Custom subtrigger gap allowed
lastLat = subTriggerPoints(find(subTriggerGaps(subTriggerPoints(2:end)>=data.max)>gap,1))+2;
if isempty(lastLat)
    lastLat = subTriggerPoints(end);
end
firstLat = firstLat+18;
lastLat = max(firstLat+1,lastLat+18);
wdw = [firstLat, lastLat];

if isempty(psthFig)
    psthFig = figure;
else
    figure(psthFig)
end
hold on
histogram(psth.Data(find(psth.Data>firstLat & psth.Data<lastLat)),'BinWidth',1,'FaceColor','yellow');
hold off

data.gap = gap;
data.thr = thresh;

end

