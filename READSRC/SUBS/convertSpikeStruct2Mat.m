function [spikeMat,stimGrid] = convertSpikeStruct2Mat(spikeStruct,stimGrid)
%convertSpikeStruct2Mat Summary of this function goes here
%   Detailed explanation goes here

if isstruct(spikeStruct)
    nSweeps = 1;
    spikeStruct = squeeze(struct2cell(spikeStruct))';
else
    nSweeps = unique(cellfun(@length,spikeStruct));
end
emptyStructs = cellfun(@isempty,spikeStruct);
spikeStruct(emptyStructs) = [];
stimGrid(:,emptyStructs)=[];
% Correct for uneven number of sweeps given
if length(nSweeps) > 1
    if min(nSweeps) == 0 % indicates unfinished sweeps
        nSweeps = max(cellfun(@length,spikeStruct));
        stimGrid(:,cellfun(@length,spikeStruct)<nSweeps)=[];
        spikeStruct(cellfun(@length,spikeStruct)<nSweeps)=[];
    end
    nSweeps = min(nSweeps);
    spikeStruct = cellfun(@(x) x(1:nSweeps),spikeStruct,'UniformOutput',0);
    %sweeps = cell2struct(sweepCell,'sweepData')';
end

spikeMat = {};
for ii = 1:length(spikeStruct)
    for jj = 1:numel(spikeStruct{ii})
        if numel(spikeStruct{ii}(jj).spikes)>0
            spikeMat{jj,ii} = [spikeStruct{ii}(jj).spikes.time];
        else
            spikeMat{jj,ii} = [];
        end
    end
end
if isempty(spikeMat)
    stimGrid(:,:)=[];
end

end

