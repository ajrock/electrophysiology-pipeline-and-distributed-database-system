function [data,psthFig] = readSRC(fileName)
%[data] = readSRC(fileName, fileType)
%   Generalized SRC reading function. Reads SRC file from variable filename
%   and selects organization variable from fileType. In case of ambiguous
%   variable data, will display stimgrid column names and ask for correct 
%   indices.
%   fileTypes supported:
%       - IO: searches for attenuation columns. 
%       - ITD: searches for column with ITD values (tracks against
%       ITDcenter)
%       - TC: searches for dB SPL and freq columns
%       - RRTF: searches for stim rate columns

tempData = readSRCfile(fileName);
fileParts = split(fileName,filesep);

data.sweepLength = tempData.sets(1).sweepLen;
data.paramNames = tempData.sets(1).stim.paramName;
stimGrid = iDigIntoNest(tempData,'sets.stim.paramVal');
sweeps = iDigIntoNest2(tempData,'sets.clusters.sweeps');

[spikeMat, data.stimGrid] = convertSpikeStruct2Mat(sweeps,stimGrid);
if ~isempty(spikeMat)
data.nSweeps = size(spikeMat,1);
pulseNames = {'click_n','pulses_n','pulseCount'};
nPulsesInd = find(contains(data.paramNames,pulseNames));%%%%% elec rrtf has separate pulse n numbers...
if isempty(nPulsesInd)
    warning('No index for pulses found!')
    if contains(fileParts{end},{'itd','ITD'})
        fprintf('ITD detected! Please enter the number of pulses!\n')
        data.nPulses = input('Enter the number of pulses: ');
    else
        disp('Non-ITD detected, assuming 1 pulse!')
        data.nPulses = 1;
    end
elseif length(nPulsesInd)~=1
    data.nPulses = max(data.stimGrid(nPulsesInd,1));
else
    assert(~isempty(nPulsesInd),'FAILED TO FIND N FOR PULSES!')
    data.nPulses = data.stimGrid(nPulsesInd,1);
end

if data.nPulses == 1
    data.repRate = 0;
    data.ipi = data.sweepLength;
    rrtfFlag = 0;
elseif contains(fileParts{end},'RRTF') % Just find an initial point for this
    if contains(fileName,'el','IgnoreCase',1)|contains(fileName,'SSD')
        rateNames = {'S1_IPI','S2_IPI','ipiLeft','ipiRight'};
    else
        rateNames = {'S1_clickrate','S2_clickrate','ipiLeft','ipiRight'};
    end
    rateInd = find(contains(data.paramNames,rateNames));
    assert(~isempty(rateInd),'FAILED TO FIND PULSE RATE!')
    data.repRate = max(data.stimGrid(rateInd,1));
    rrtfFlag = 1;
else
    rateNames = {'S1_clickrate','S2_clickrate','S1_freq','S2_freq','el_freq','ac_freq',...
        'ipiLeft','ipiRight'};
    rateInd = find(contains(data.paramNames,rateNames));
    if isempty(rateInd)
        warning('Failed to find a Pulse Rate!')
        data.repRate = input('Enter the repetition rate: ');
    elseif length(rateInd)==1
        data.repRate = data.stimGrid(rateInd,1);
    else
        data.repRate = max(data.stimGrid(rateInd,1));
    end
    data.ipi = 1000/data.repRate;
%     if ~isempty(rateInd) && contains(data.paramNames{rateInd(1)},'freq') 
%         data.repRate = 1000/data.repRate;
%     end
    rrtfFlag = 0;
end
if any(contains(data.paramNames,'enter'))
    del = max(data.stimGrid(contains(data.paramNames,{'delayLeft','delayRight','S1_start','S2_start'}),1:2),[],'all');
    assert(del~=0||~rrtfFlag,'You need a valid delay parameter to continue with RRTF analysis.')
    data.itdC = unique(data.stimGrid(contains(data.paramNames,'enter'),:))+del;
    [psth,psthFig,data.spikeMat,data.wdw] = windowDecisionLoop(spikeMat,data.nPulses,data.repRate,data.sweepLength,rrtfFlag,data.itdC);
else
    [psth,psthFig,data.spikeMat,data.wdw] = windowDecisionLoop(spikeMat,data.nPulses,data.ipi,data.sweepLength,rrtfFlag,[]);
end
data.fullMat = {spikeMat};
else
    psthFig =figure();
    data.spikeMat={};
end

end